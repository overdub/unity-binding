using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DataBinding.Properties;
using DataBinding.Utils;
#if UNITY_EDITOR
using DataBinding.Lib.CodeGenerator;
using System.IO;
using System.Reflection;
using Templates;
using UnityEditor;
#endif
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding
{
    [ExecuteAlways]
    public class DataBindings : MonoBehaviour
    {
        public BindingsList bindings = new BindingsList();
        private bool isEnabled;

        private void OnEnable()
        {
            isEnabled = true;
            bindings.Watch(transform);
        }

        private void OnDisable()
        {
            isEnabled = false;
            bindings.Unwatch();
        }

        private void OnTransformParentChanged()
        {
            if (!isEnabled) return;
            OnDisable();
            OnEnable();
        }

        private void OnDestroy()
        {
            bindings.Unwatch();
        }

        public void Add(Object target, string targetMember)
        {
            var binding = new Binding(target, targetMember);
            binding.AutoFillModelProperty(target is GameObject go ? go.transform : (target as Component).transform);
            bindings.bindings.Add(binding);
        }

#if UNITY_EDITOR
        [SerializeField, HideInInspector] private string generatedController;

        private string JoinBlocks(params List<string>[] blocks)
        {
            var ret = "";
            foreach (var block in blocks)
            {
                ret += string.Join("\n", block) + (block.Count > 0 ? "\n\n" : "");
            }

            return ret.TrimEnd('\n') + "\n";
        }

        [ContextMenu("Generate Controller... #%g")]
        private void GenerateController()
        {
            var generatedClassName = gameObject.name.Substring(0, 1).ToUpper() + gameObject.name.Substring(1) + "Controller";
            var defaultDirectory = Path.Combine(Application.dataPath, "Scripts", "Controller");
            if (!Directory.Exists(defaultDirectory))
            {
                defaultDirectory = Path.Combine(Application.dataPath, "Scripts");
            }

            if (!Directory.Exists(defaultDirectory))
            {
                defaultDirectory = Application.dataPath;
            }

            var controllerDirectoryKey = "DataBindingControllerDirectory";
            var directory = EditorPrefs.GetString(controllerDirectoryKey, defaultDirectory);
            var fileName = EditorUtility.SaveFilePanel("Save generated class as...", directory, generatedClassName, "cs");
            if (string.IsNullOrEmpty(fileName)) return;

            var filePath = Path.GetDirectoryName(fileName);
            EditorPrefs.SetString(controllerDirectoryKey, filePath);

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            var controllerClassTemplate = CodeGenerator.LoadTemplate(typeof(ControllerClassTemplate));
            var controllerClassParser = new TemplateParser(controllerClassTemplate);
            var boundPropertyParser = new TemplateParser(controllerClassParser.FindBlock("BoundProperty"));
            var cachedComponentParser = new TemplateParser(controllerClassParser.FindBlock("PrivateField"));
            var cachedComponentExpressionParser = new TemplateParser(controllerClassParser.FindBlock("CachedComponentExpression"));
            var bindingChangeMethodParser = new TemplateParser(controllerClassParser.FindBlock("BindingChangeMethod"));

            var namespaces = controllerClassParser
                .FindBlock("Using")
                .Split('\n')
                .Select(v => v.Replace("using", "").Replace(";", "").Trim())
                .Where(v => !string.IsNullOrEmpty(v))
                .ToList();
            var boundPropertyNames = new List<string>();
            var boundProperties = new List<string>();
            var cachedComponents = new List<string>();
            var cacheComponentExpressions = new List<string>();
            var cachedComponentNames = new List<string>();
            var bindingChangeMethodNames = new List<string>();
            var bindingChangeMethods = new List<string>();

            bindings.bindings.ForEach(binding =>
            {
                var modelProperty = binding.ModelProperty;

                if (modelProperty.Model == null) return;

                var modelType = modelProperty.Model.GetType();
                namespaces.Add(modelType.Namespace);

                var propertyType = modelProperty.PropertyType;
                if (!BindingReflectionUtils.IsSystemType(propertyType))
                {
                    namespaces.Add(propertyType.Namespace);
                }

                var attributePropertyName = $"nameof({modelType.Name}.{modelProperty.PropertyName})";
                var boundPropertyName = CodeGenerator.UniqueName(CodeGenerator.LowerFirst(modelProperty.PropertyName), boundPropertyNames);
                boundPropertyNames.Add(boundPropertyName);

                boundPropertyParser.Revert();
                boundPropertyParser.ReplaceBlocks(
                    ("PropertyName", attributePropertyName),
                    ("Type", BindingReflectionUtils.GetTypeAlias(propertyType)),
                    ("Name", boundPropertyName)
                );
                boundProperties.Add(boundPropertyParser.Contents);

                var targetType = binding.Accessor.Target.GetType();
                var componentVariableName = "";

                if (targetType == typeof(GameObject))
                {
                    componentVariableName = "gameObject";
                } else if (targetType == typeof(Transform))
                {
                    componentVariableName = "transform";
                } else
                {
                    var componentIndex = ObjectUtils.ExtractComponentIndex(binding.Accessor.TargetObject.ComponentPath);
                    var shortComponentTypeName = targetType.Name;

                    componentVariableName = CodeGenerator.LowerFirst(shortComponentTypeName) + (componentIndex > 0 ? componentIndex.ToString() : "");
                    if (!cachedComponentNames.Contains(componentVariableName))
                    {
                        cachedComponentNames.Add(componentVariableName);

                        namespaces.Add(targetType.Namespace);

                        cachedComponentParser.Revert();
                        cachedComponentParser.ReplaceBlocks(
                            ("Type", shortComponentTypeName),
                            ("Name", componentVariableName)
                        );
                        cachedComponents.Add(cachedComponentParser.Contents);

                        cachedComponentExpressionParser.Revert();
                        cachedComponentExpressionParser.ReplaceBlocks(
                            ("Name", componentVariableName),
                            ("Type", shortComponentTypeName)
                        );
                        cacheComponentExpressions.Add(cachedComponentExpressionParser.Contents);
                    }
                }

                var methodName = CodeGenerator.UniqueName(CodeGenerator.UpperFirst(modelProperty.PropertyName) + "Changed", bindingChangeMethodNames);
                bindingChangeMethodNames.Add(methodName);

                var targetMemberInfo = binding.Accessor.MemberInfo;
                var methodBody = "";
                if (targetMemberInfo != null)
                {
                    if (targetMemberInfo is FieldInfo fieldInfo)
                    {
                        methodBody = $"{componentVariableName}.{fieldInfo.Name} = {boundPropertyName}.Value;";
                    }

                    if (targetMemberInfo is PropertyInfo propertyInfo)
                    {
                        methodBody = $"{componentVariableName}.{propertyInfo.Name} = {boundPropertyName}.Value;";
                    }

                    if (targetMemberInfo is MethodInfo methodInfo)
                    {
                        methodBody = $"{componentVariableName}.{methodInfo.Name}({boundPropertyName}.Value);";
                    }
                }

                bindingChangeMethodParser.Revert();
                bindingChangeMethodParser.ReplaceBlocks(
                    ("PropertyName", attributePropertyName),
                    ("Name", methodName),
                    ("Body", methodBody)
                );
                bindingChangeMethods.Add(bindingChangeMethodParser.Contents);
            });

            namespaces = namespaces.Distinct().Select(u => "using " + u + ";").ToList();
            namespaces.Sort();

            var scriptPath = (new Uri(Application.dataPath)).MakeRelativeUri(new Uri(filePath)).ToString();
            scriptPath = new Regex("^Assets" + Path.DirectorySeparatorChar).Replace(scriptPath, "");
            scriptPath = new Regex("^Scripts" + Path.DirectorySeparatorChar).Replace(scriptPath, "").Trim(Path.DirectorySeparatorChar);
            scriptPath = scriptPath.Replace(Path.DirectorySeparatorChar.ToString(), ".");
            scriptPath = new Regex("[^\\w.]").Replace(scriptPath, "");
            var controllerNamespace = string.IsNullOrEmpty(scriptPath) ? "Controllers" : scriptPath;

            var className = Path.GetFileNameWithoutExtension(fileName);

            controllerClassParser.ReplaceBlocks(
                ("Using", string.Join("\n", namespaces)),
                ("Namespace", controllerNamespace),
                ("ClassName", "class " + className),
                ("FieldsAndMethods", JoinBlocks(boundProperties, cachedComponents, bindingChangeMethods)),
                ("CachedComponentExpression", string.Join("\n", cacheComponentExpressions))
            );
            File.WriteAllText(fileName, controllerClassParser.Contents);

            generatedController = controllerNamespace + "." + className;

            AssetDatabase.Refresh();
        }
#endif
    }
}