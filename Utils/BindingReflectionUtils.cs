using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Utils
{
    public static class BindingReflectionUtils
    {
        public static IEnumerable<FieldInfo> GetAllFields(Type targetType, Func<FieldInfo, bool> predicate = null, bool includeBaseTypes = true, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)
        {
            List<Type> types = new List<Type>()
            {
                targetType
            };

            if (includeBaseTypes)
            {
                while (types.Last().BaseType != null)
                {
                    types.Add(types.Last().BaseType);
                }
            }

            foreach (var type in types)
            {
                IEnumerable<FieldInfo> fieldInfos = type.GetFields(bindingFlags);
                if (predicate != null)
                {
                    fieldInfos = fieldInfos.Where(predicate);
                }

                foreach (var fieldInfo in fieldInfos)
                {
                    yield return fieldInfo;
                }
            }
        }
        
        public static IEnumerable<MethodInfo> GetAllMethods(Type targetType, Func<MethodInfo, bool> predicate = null, bool includeBaseTypes = true, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)
        {
            List<Type> types = new List<Type>()
            {
                targetType
            };

            if (includeBaseTypes)
            {
                while (types.Last().BaseType != null)
                {
                    types.Add(types.Last().BaseType);
                }
            }

            foreach (var type in types)
            {
                IEnumerable<MethodInfo> methodInfos = type.GetMethods(bindingFlags);
                if (predicate != null)
                {
                    methodInfos = methodInfos.Where(predicate);
                }

                foreach (var info in methodInfos)
                {
                    yield return info;
                }
            }
        }

        public static IEnumerable<FieldInfo> GetAllSerializedFields(Type target)
        {
            return GetAllFields(target, info => info.IsPublic || info.GetCustomAttribute(typeof(SerializeField)) != null, true);
        }

        public static IEnumerable<FieldInfo> GetAllInspectorFields(Type target)
        {
            return GetAllFields(target, info => info.IsPublic, true);
        }

        public static IEnumerable<FieldInfo> GetAllInspectorFields(Type target, Type[] types)
        {
            if (types == null)
            {
                return GetAllInspectorFields(target);
            }

            return GetAllFields(target, info => info.IsPublic && types.Contains(info.FieldType), true);
        }

        public static FieldInfo GetField(Type target, string fieldName)
        {
            return GetAllFields(target, f => f.Name.Equals(fieldName, StringComparison.InvariantCulture)).FirstOrDefault();
        }


        public static IEnumerable<PropertyInfo> GetAllProperties(Type targetType, Func<PropertyInfo, bool> predicate = null, bool includeBaseTypes = true, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)
        {
            List<Type> types = new List<Type>()
            {
                targetType
            };

            while (types.Last().BaseType != null)
            {
                types.Add(types.Last().BaseType);
            }

            foreach (var type in types)
            {
                IEnumerable<PropertyInfo> propertyInfos = type.GetProperties(bindingFlags);
                if (predicate != null)
                {
                    propertyInfos = propertyInfos.Where(predicate);
                }

                foreach (var propertyInfo in propertyInfos)
                {
                    yield return propertyInfo;
                }
            }
        }

        public static IEnumerable<PropertyInfo> GetAllPublicProperties(Type targetType, bool includeBaseTypes = true)
        {
            return GetAllProperties(targetType, null, includeBaseTypes, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
        }

        public static IEnumerable<PropertyInfo> GetAllPublicProperties(Type targetType, Type type)
        {
            return GetAllProperties(targetType, info => info.PropertyType == type, true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
        }

        public static IEnumerable<PropertyInfo> GetAllPublicProperties(Type targetType, Type[] types)
        {
            if (types == null)
            {
                return GetAllPublicProperties(targetType);
            }

            return GetAllProperties(targetType, info => types.Contains(info.PropertyType), true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
        }

        public static IEnumerable<MethodInfo> GetSetters(Type targetType, Func<MethodInfo, bool> predicate = null, bool includeBaseTypes = true, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)
        {
            List<Type> types = new List<Type>()
            {
                targetType
            };

            while (types.Last().BaseType != null)
            {
                types.Add(types.Last().BaseType);
            }

            foreach (var type in types)
            {
                IEnumerable<MethodInfo> methodInfos = type.GetMethods(bindingFlags);
                if (predicate != null)
                {
                    methodInfos = methodInfos.Where(predicate);
                }

                foreach (var methodInfo in methodInfos)
                {
                    yield return methodInfo;
                }
            }
        }

        public static IEnumerable<MethodInfo> GetAllPublicSetters(Type targetType, Type[] types)
        {
            return GetSetters(targetType, info =>
                {
                    if (types == null) return false;

                    var parameters = info.GetParameters();
                    return parameters.Length == 1 && parameters[0] != null && types.Contains(parameters[0].ParameterType) && !info.Name.StartsWith("set_");
                }, true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
        }

        public static List<MemberInfo> GetMembersByType(Type targetType, Type type)
        {
            List<Type> types = null;

            if (type != null)
            {
                types = new List<Type>()
                {
                    type,
                    typeof(string)
                };

                if (type.IsEnum)
                {
                    types.Add(typeof(int));
                    types.Add(typeof(float));
                    types.Add(typeof(double));
                }

                if (type == typeof(bool))
                {
                    types.Add(typeof(int));
                    types.Add(typeof(float));
                }
            }

            var typeArray = types?.ToArray();

            var result = GetAllInspectorFields(targetType, typeArray).ToList<MemberInfo>();
            result.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));

            var properties = GetAllPublicProperties(targetType, typeArray).ToList<MemberInfo>();
            properties.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));
            result.AddRange(properties);

            var setters = GetAllPublicSetters(targetType, typeArray).ToList<MemberInfo>();
            setters.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));
            result.AddRange(setters);

            return result;
        }

        public static List<MemberInfo> GetAllMembers(Type targetType)
        {
            return GetMembersByType(targetType, null);
        }

        public static MemberInfo GetMemberByName(Type targetType, Type preferredType, string name)
        {
            if (targetType == null)
            {
                return null;
            }

            List<Type> types = null;
            if (preferredType != null)
            {
                types = new List<Type>()
                {
                    preferredType,
                    typeof(string)
                };

                if (preferredType.IsEnum)
                {
                    types.Add(typeof(int));
                    types.Add(typeof(float));
                    types.Add(typeof(double));
                }

                if (preferredType == typeof(bool))
                {
                    types.Add(typeof(int));
                }
            }

            var fields = GetAllFields(targetType, info => info.IsPublic && info.Name == name && (types == null || types.Contains(info.FieldType)), true).ToList();
            if (fields.Count > 0) return fields[0];

            var properties = GetAllProperties(targetType, info => info.CanWrite && info.Name == name && (types == null || types.Contains(info.PropertyType)), true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).ToList();
            if (properties.Count > 0) return properties[0];

            var setters = GetSetters(targetType, info =>
                {
                    if (info.IsGenericMethod || info.Name != name) return false;

                    var parameters = info.GetParameters();
                    return parameters != null && parameters.Length == 1 && parameters[0] != null && (types == null || types.Contains(parameters[0].ParameterType));
                }, true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).ToList();
            if (setters.Count > 0) return setters[0];

            return null;
        }

        public static Type GetMemberOutputType(MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return propertyInfo.PropertyType;
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                return methodInfo.ReturnType;
            }

            return null;
        }
        
        public static Type GetMemberInputType(MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return propertyInfo.PropertyType;
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                var methodParameters = methodInfo.GetParameters();
                return methodParameters.Length == 1 ? methodParameters[0].ParameterType : null;
            }

            return null;
        }

        public static Func<object> CreateUntypedModelPropertyGetter(IModel model, string propertyName)
        {
            var propertyInfo = model?.GetPropertyInfo(propertyName);

            if (propertyInfo == null)
            {
                Debug.LogWarning($"Property {propertyName} not found.");
                return () => default;
            }

            var target = model.GetPropertyTarget(propertyName);

            return () => propertyInfo.GetValue(target);
        }
        
        public static Func<T> CreateTypedModelPropertyGetter<T>(IModel model, string propertyName, bool returnNull = false)
        {
            var propertyInfo = model?.GetPropertyInfo(propertyName);

            if (propertyInfo == null)
            {
                Debug.LogWarning($"Property {propertyName} not found.");
                return () => default;
            }

            var target = model.GetPropertyTarget(propertyName);

            return (Func<T>) Delegate.CreateDelegate(typeof(Func<T>), target, propertyInfo.GetGetMethod());
        }

        public static Action<T> CreateTypedModelPropertySetter<T>(IModel model, string propertyName)
        {
            var propertyInfo = model?.GetPropertyInfo(propertyName);

            if (propertyInfo == null)
            {
                Debug.LogWarning($"Property {propertyName} not found.");
                return value => { };
            }

            if (!propertyInfo.CanWrite)
            {
                return value => { };
            }

            var target = model.GetPropertyTarget(propertyName);

            return (Action<T>) Delegate.CreateDelegate(typeof(Action<T>), target, propertyInfo.GetSetMethod());
        }

        public static Action<TS> CreateTypedConversionSetter<TS, TD>(object target, MemberInfo memberInfo, Func<TS, TD> converter)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return value => fieldInfo.SetValue(target, converter(value));
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                var propertyDelegate = (Action<TD>) Delegate.CreateDelegate(typeof(Action<TD>), target, propertyInfo.GetSetMethod());
                return value => propertyDelegate(converter(value));
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                if (methodInfo.ReturnParameter != null)
                {
                    var parameters = new object[1];

                    return value =>
                    {
                        parameters[0] = converter(value);
                        methodInfo.Invoke(target, parameters);
                    };
                }

                var methodDelegate = (Action<TD>) methodInfo.CreateDelegate(typeof(Action<TD>), target);
                return value => methodDelegate(converter(value));
            }

            return value => { };
        }

        public static Func<TS> CreateTypedGetter<TS>(object target, MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return () => (TS)fieldInfo.GetValue(target);
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return (Func<TS>) Delegate.CreateDelegate(typeof(Func<TS>), target, propertyInfo.GetGetMethod());
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                return (Func<TS>) methodInfo.CreateDelegate(typeof(Func<TS>), target);
            }

            return () => default;
        }
        
        public static Action<TS> CreateTypedSetter<TS>(object target, MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return value => fieldInfo.SetValue(target, value);
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return (Action<TS>) Delegate.CreateDelegate(typeof(Action<TS>), target, propertyInfo.GetSetMethod());
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                if (methodInfo.ReturnParameter != null)
                {
                    var parameters = new object[1];

                    return value =>
                    {
                        parameters[0] = value;
                        methodInfo.Invoke(target, parameters);
                    };
                }

                return (Action<TS>) methodInfo.CreateDelegate(typeof(Action<TS>), target);
            }

            return value => { };
        }

        public static Func<object> CreateUntypedGetter(object target, MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                return () => fieldInfo.GetValue(target);
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                return () => propertyInfo.GetValue(target);
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                return () => methodInfo.Invoke(target, null);
            }

            return () => default;
        }
        
        public static Action<object> CreateUntypedSetter(Type sourceType, object target, MemberInfo memberInfo)
        {
            if (memberInfo is FieldInfo fieldInfo)
            {
                if (fieldInfo.FieldType == typeof(string) && sourceType != typeof(string))
                {
                    return value => fieldInfo.SetValue(target, value.ToString());
                }

                if (fieldInfo.FieldType != sourceType && typeof(IConvertible).IsAssignableFrom(fieldInfo.FieldType))
                {
                    return value => fieldInfo.SetValue(target, Convert.ChangeType(value, fieldInfo.FieldType));
                }

                return value => fieldInfo.SetValue(target, value);
            }

            if (memberInfo is PropertyInfo propertyInfo)
            {
                if (propertyInfo.PropertyType == typeof(string) && sourceType != typeof(string))
                {
                    return value => propertyInfo.SetValue(target, value.ToString());
                }

                if (propertyInfo.PropertyType != sourceType && typeof(IConvertible).IsAssignableFrom(propertyInfo.PropertyType))
                {
                    return value => propertyInfo.SetValue(target, Convert.ChangeType(value, propertyInfo.PropertyType));
                }

                return value => propertyInfo.SetValue(target, value);
            }

            if (memberInfo is MethodInfo methodInfo)
            {
                var parameters = new object[1];
                var parameterType = methodInfo.GetParameters()[0].ParameterType;

                if (parameterType == typeof(string) && sourceType != typeof(string))
                {
                    return value =>
                    {
                        parameters[0] = value.ToString();
                        methodInfo.Invoke(target, parameters);
                    };
                }

                if (parameterType != sourceType && typeof(IConvertible).IsAssignableFrom(parameterType))
                {
                    return value =>
                    {
                        parameters[0] = Convert.ChangeType(value, parameterType);
                        methodInfo.Invoke(target, parameters);
                    };
                }

                return value =>
                {
                    parameters[0] = value;
                    methodInfo.Invoke(target, parameters);
                };
            }

            return value => { };
        }

        public static string GetPropertyPathRoot(string propertyPath)
        {
            if (propertyPath == null) return null;

            var index = propertyPath.IndexOf(".", StringComparison.Ordinal);
            return index == -1 ? propertyPath : propertyPath.Substring(0, index);
        }

        public static string GetPropertyNameFromField(string fieldName)
        {
            var propertyName = fieldName.StartsWith("m_") ? fieldName.Substring(2) : (fieldName.StartsWith("_") ? fieldName.Substring(1) : fieldName);
            return propertyName.Substring(0, 1).ToUpper() + propertyName.Substring(1);
        }

        public static string GetLowerCasePropertyNameFromField(string fieldName)
        {
            var propertyName = fieldName.StartsWith("m_") ? fieldName.Substring(2) : (fieldName.StartsWith("_") ? fieldName.Substring(1) : fieldName);
            return propertyName.Substring(0, 1).ToLower() + propertyName.Substring(1);
        }

        public static string GetPropertyNameFromPath(string propertyPath)
        {
            return GetPropertyNameFromField(GetPropertyPathRoot(propertyPath));
        }

        public static string GetLowerCasePropertyNameFromPath(string propertyPath)
        {
            return GetLowerCasePropertyNameFromField(GetPropertyPathRoot(propertyPath));
        }

        public static MemberInfo FindWritableMember(Type targetType, string propertyPath)
        {
            return GetMemberByName(targetType, null, GetPropertyNameFromPath(propertyPath))
                   ?? GetMemberByName(targetType, null, GetLowerCasePropertyNameFromPath(propertyPath));
        }
        
        public static List<Type> GetAllBaseTypes(Type type)
        {
            var types = new List<Type>();
            var t = type;
            while (t != null)
            {
                types.Add(t);
                t = t.BaseType;
            }

            return types;
        }
        
        public static bool ExtendsType(Type type, Type baseType)
        {
            var t = type;
            while (t != null)
            {
                if (t == baseType) return true;
                t = t.BaseType;
            }

            return false;
        }

        private static readonly Dictionary<Type, string> Aliases = new Dictionary<Type, string>
        {
            {typeof(byte), "byte"},
            {typeof(sbyte), "sbyte"},
            {typeof(short), "short"},
            {typeof(ushort), "ushort"},
            {typeof(int), "int"},
            {typeof(uint), "uint"},
            {typeof(long), "long"},
            {typeof(ulong), "ulong"},
            {typeof(float), "float"},
            {typeof(double), "double"},
            {typeof(decimal), "decimal"},
            {typeof(object), "object"},
            {typeof(bool), "bool"},
            {typeof(char), "char"},
            {typeof(string), "string"},
            {typeof(void), "void"}
        };

        public static string GetTypeAlias(Type type)
        {
            return Aliases.ContainsKey(type) ? Aliases[type] : type.Name;
        }

        public static bool IsSystemType(Type type)
        {
            return Aliases.ContainsKey(type);
        }
    }
}