
using System;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Utils
{
    public static class MemberAccessorUtils
    {
        public static MemberInfo ParsePath(GameObject context, string path, out Object target)
        {
            // parse Component property reference
            if (path.StartsWith("<"))
            {
                target = null;
                
                var index = path.IndexOf(">");
                if (index == -1)
                {
                    Debug.LogWarning($"Failed to parse {path}");
                    return null;
                }
                
                var targetTypeName = path.Substring(1, index - 1);
                var propertyName = path.Length > index + 2 ? path.Substring(index + 2) : "";

                var componentType = Type.GetType(targetTypeName);
                if (componentType == null)
                {
                    Debug.LogWarning($"Component type {targetTypeName} not found {path}");
                    return null;
                }

                var component = context.GetComponent(componentType);
                if (component == null)
                {
                    Debug.LogWarning($"Component not found {path}");
                    return null;
                }

                target = component;
                return BindingReflectionUtils.GetMemberByName(componentType, null, propertyName);
            }

            // parse GameObject property reference
            target = context;
            return BindingReflectionUtils.GetMemberByName(context.GetType(), null, path);
        }
    }
}