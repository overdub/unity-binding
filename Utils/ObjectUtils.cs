using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DataBinding.Utils
{
    public static class ObjectUtils
    {
        private static List<Component> components = new List<Component>();
        
        public static string GetAbsolutePath(Transform transform)
        {
            if (transform.transform.parent == null)
            {
                return "/" + transform.name;
            }
            
            return GetAbsolutePath(transform.transform.parent) + "/" + transform.name;
        }

        public static string GetRelativePath(string basePath, string path)
        {
            return (new Uri(path)).MakeRelativeUri(new Uri(basePath)).LocalPath;
        }

        public static string GetFullPath(string basePath, string relativePath)
        {
            return Path.GetFullPath(Path.Combine(basePath, relativePath));
        }
        
        public static GameObject ResolvePath(Transform context, string relativePath)
        {
            if (context == null)
            {
                return null;
            }
            
            var targetGameObject = context.gameObject;
            
            if (! string.IsNullOrEmpty(relativePath))
            {
                var fullPath = GetFullPath(ObjectUtils.GetAbsolutePath(context), relativePath);
                Debug.Log(fullPath);
                targetGameObject = GameObject.Find(fullPath);

                if (targetGameObject == null)
                {
                    return null;
                }
            }
            
            return targetGameObject;
        }

        public static Component ResolveComponentPath(Transform context, string componentTypeName, int componentIndex, string assemblyName)
        {
            if (context == null || string.IsNullOrEmpty(componentTypeName))
            {
                return null;
            }
            
            var componentType = Type.GetType(componentTypeName + ", " + assemblyName);
            if (componentType == null)
            {
                return null;
            }

            if (componentIndex <= 0)
            {
                return context.GetComponent(componentType);
            }

            var components = context.GetComponents(componentType);
            if (componentIndex >= components.Length)
            {
                return null;
            }

            return components[componentIndex];
        }
        
        public static Component ResolveComponentPath(Transform context, string componentTypeName, string assemblyName)
        {
            if (componentTypeName.EndsWith("]"))
            {
                var type = componentTypeName.Substring(0, componentTypeName.IndexOf("["));
                var index = int.Parse(componentTypeName.Substring(componentTypeName.IndexOf("[") + 1).TrimEnd(']'));
                return ResolveComponentPath(context, type, index, assemblyName);
            }
            
            return ResolveComponentPath(context, componentTypeName, 0, assemblyName);
        }

        public static string ExtractComponentTypeName(string componentPath)
        {
            if (string.IsNullOrEmpty(componentPath))
            {
                return "";
            }
            
            if (componentPath.EndsWith("]"))
            {
                return componentPath.Substring(0, componentPath.IndexOf("["));
            }

            return componentPath;
        }
        
        public static int ExtractComponentIndex(string componentPath)
        {
            if (componentPath.EndsWith("]"))
            {
                var index = int.Parse(componentPath.Substring(componentPath.IndexOf("[") + 1).TrimEnd(']'));
                return index;
            }

            return -1;
        }

        public static Type ResolveComponentType(string componentPath, string assemblyName)
        {
            return Type.GetType(ExtractComponentTypeName(componentPath) + ", " + assemblyName);
        }
        
        public static string GetComponentPath(Component component)
        {
            var type = component.GetType();
            component.transform.GetComponents(type, components);
            return type.FullName + (components.Count > 1 ? $"[{components.IndexOf(component)}]" : "");
        }

        public static string GetComponentAssembly(Component component)
        {
            var type = component.GetType();
            return type.Assembly.FullName;
        }
    }
}