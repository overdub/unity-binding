using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DataBinding.Utils
{
    public static class BindingUtils
    {
        public static IModel GetModel(Transform context, string propertyName)
        {
            if (context == null) return null;

            var models = context.GetComponents<IModel>();
            foreach (var model in models)
            {
                if (model != null && model.Has(propertyName))
                {
                    return model;
                }
            }

            if (context.parent == null)
            {
                return GlobalModel.GetModel(propertyName);
            }

            return GetModel(context.parent, propertyName);
        }

        public static IModel GetModel(Component context, string propertyName)
        {
            if (context == null) return null;

            return GetModel(context.transform, propertyName);
        }

        private static List<IModel> GetGlobalModels()
        {
            return GlobalModel.Instances;
        }

        public static List<IModel> GetParentModels(Transform context)
        {
            if (context == null)
            {
                return new List<IModel>();
            }

            return context.GetComponentsInParent<IModel>().Concat(GetGlobalModels()).ToList();
        }

        public static List<string> GetAvailableProperties(Transform context)
        {
            var parentModels = GetParentModels(context);
            return parentModels.ToList().SelectMany(m => m.PropertyNames).ToList();
        }

        public static List<string> GetAvailableProperties(Transform context, Type type)
        {
            var parentModels = GetParentModels(context);
            return parentModels
                .ToList()
                .SelectMany(m => m.PropertyNames.Where(name => BindingReflectionUtils.ExtendsType(m.GetPropertyType(name), type)))
                .ToList();
        }
    }
}