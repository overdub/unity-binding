using System;

namespace DataBinding.Lib.CodeGenerator
{
    public class TemplateParser
    {
        private string template;
        private string contents;

        public string Contents => contents;
        
        public TemplateParser(string template)
        {
            this.template = template;
            Revert();
        }

        public void Revert()
        {
            contents = template;
        }

        private string GetStartTag(string name)
        {
            return $"/*<{name}>*/";
        }
        
        private string GetEndTag(string name)
        {
            return $"/*</{name}>*/";
        }

        public string FindBlock(string name, out int startIndex, out int endIndex)
        {
            var startTag = GetStartTag(name);
            startIndex = contents.IndexOf(startTag);
            endIndex = startIndex;
            if (startIndex == -1) return null;
            
            var endTag = GetEndTag(name);
            endIndex = contents.IndexOf(endTag, startIndex) + endTag.Length;
            if (endIndex == -1)
            {
                throw new ArgumentException($"Closing tag /\"{name}\" not found");
            }

            return contents.Substring(startIndex + startTag.Length, endIndex - endTag.Length - (startIndex + startTag.Length)).Trim('\n');
        }

        public void ReplaceBlock(int startIndex, int endIndex, string replacement)
        {
            if (startIndex < 0 || startIndex >= contents.Length || endIndex < 0 || endIndex >= contents.Length || startIndex > endIndex)
            {
                throw new ArgumentException("Invalid indices");
            }
            contents = contents.Substring(0, startIndex) + replacement + contents.Substring(endIndex);
        }

        public string FindBlock(string name)
        {
            var startIndex = -1;
            var endIndex = -1;
            var block = FindBlock(name, out startIndex, out endIndex);
            return block;
        }

        public string ReplaceBlock(string name, string replacement)
        {
            var startIndex = -1;
            var endIndex = -1;
            var block = FindBlock(name, out startIndex, out endIndex);

            ReplaceBlock(startIndex, endIndex, replacement);
            return block;
        }

        public void ReplaceBlocks(params (string, string)[] replacements)
        {
            foreach (var replacement in replacements)
            {
                ReplaceBlock(replacement.Item1, replacement.Item2);
            }
        }
    }
}