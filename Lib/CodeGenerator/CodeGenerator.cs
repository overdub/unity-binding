using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DataBinding.Lib.CodeGenerator
{
    public class CodeGenerator
    {
#if UNITY_EDITOR
        private static string GetMonoScriptPathFor(Type type)
        {
            var guids = AssetDatabase.FindAssets($"{type.Name} t:script");
            if (guids.Length == 0)
            {
                throw new IOException($"Cannot find script file for {type.Name}");
            }

            return AssetDatabase.GUIDToAssetPath(guids[0]);
        }

        public static void SaveScript([NotNull] string outputPath, string contents)
        {
            File.WriteAllText(outputPath, contents.Replace("\r\n", "\n"));

            AssetDatabase.Refresh();
        }

        public static string LoadTemplate([NotNull] Type type)
        {
            var templatePath = GetMonoScriptPathFor(type);
            if (!File.Exists(templatePath))
            {
                throw new IOException("Template file no found");
            }

            return File.ReadAllText(templatePath).Replace("\r\n", "\n");
        }
#endif

        public static string LowerFirst(string value)
        {
            return value.Length < 2 ? value.ToLower() : value.Substring(0, 1).ToLower() + value.Substring(1);
        }

        public static string UpperFirst(string value)
        {
            return value.Length < 2 ? value.ToUpper() : value.Substring(0, 1).ToUpper() + value.Substring(1);
        }

        public static string UniqueName(string name, [NotNull] List<string> cachedNames)
        {
            var existingNames = cachedNames.Where(n => n.StartsWith(name));
            var count = existingNames.Count();

            return name + (count > 0 ? count.ToString() : "");
        }
    }
}