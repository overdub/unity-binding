using System;
using System.Linq;
using UnityEditor.Compilation;

namespace DataBinding.Lib.Editor
{
    public static class EditorUtils
    {
        public static Type GetTypeFromName(string name)
        {
            var type = Type.GetType(name);
            if (type != null)
            {
                return type;
            }
            
            var assemblies = CompilationPipeline.GetAssemblies().ToList();
            foreach (var assembly in assemblies)
            {
                type = Type.GetType(name + ", " + assembly.name);
                if (type != null)
                {
                    return type;
                }
            }

            return null;
        }
    }
}