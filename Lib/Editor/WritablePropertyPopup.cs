using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Utils;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Lib.Editor
{
    public static class WritablePropertyPopup
    {
        public static void Draw(Rect position, Type targetType, Type restrictedType, SerializedProperty property, string placeholder = "")
        {
            Draw(position, "", targetType, restrictedType, property, placeholder);
        }

        public static void Draw(Rect position, string label, Type targetType, Type restrictedType, SerializedProperty property, string placeholder = "")
        {
            var targetMembers = targetType != null ? BindingReflectionUtils.GetMembersByType(targetType, restrictedType) : new List<MemberInfo>();
            var targetMemberNames = targetMembers.Select(info => info.Name).ToList();

            EditablePopup.Draw(position, new GUIContent(label), property, targetMemberNames, placeholder);
        }
    }
}