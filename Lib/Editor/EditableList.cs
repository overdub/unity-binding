using System;
using System.Collections;
using System.Collections.Generic;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace DataBinding.Lib.Editor
{
    public class EditableList<T> : ReorderableList
    {
        public event Action beforeAdd;
        public event Action<int> beforeDelete;

        [Serializable]
        private class PropertyList
        {
            public List<T> items = new List<T>();
        }

        private readonly string displayName;

        public EditableList(string displayName, IList elements) : base(elements, typeof(T))
        {
            this.displayName = displayName;
            Init();
        }

        public EditableList(string displayName, IList elements, bool draggable, bool displayHeader, bool displayAddButton, bool displayRemoveButton) : base(elements, typeof(T), draggable, displayHeader, displayAddButton, displayRemoveButton)
        {
            this.displayName = displayName;
            Init();
        }

        public EditableList(SerializedObject serializedObject, SerializedProperty elements) : base(serializedObject, elements)
        {
            displayName = serializedProperty.displayName;
            Init();
        }

        public EditableList(SerializedObject serializedObject, SerializedProperty elements, bool draggable, bool displayHeader, bool displayAddButton, bool displayRemoveButton) : base(serializedObject, elements, draggable, displayHeader, displayAddButton, displayRemoveButton)
        {
            displayName = serializedProperty.displayName;
            Init();
        }

        private void Init()
        {
            drawHeaderCallback = DrawHeaderCallback;
            drawElementCallback = DrawElementCallback;
            elementHeightCallback = ElementHeightCallback;
            onAddCallback = OnAddCallback;
            onRemoveCallback = OnRemoveCallback;
        }

        private void DrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, displayName, EditorStyles.label);
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            var element = serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 1.0f;

            // set index to clicked row
            // TODO: Detect tab focus change
            if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
            {
                this.index = index;
            }

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(rect, element, GUIContent.none);
            if (EditorGUI.EndChangeCheck())
            {
                this.index = index;
            }
        }

        private float ElementHeightCallback(int index)
        {
            return EditorGUI.GetPropertyHeight(serializedProperty.GetArrayElementAtIndex(index)) + 4.0f;
        }

        private void DisposeAll()
        {
            if (!typeof(IDisposable).IsAssignableFrom(typeof(T))) return;

            for (var i = 0; i < serializedProperty.arraySize; i++)
            {
                var property = serializedProperty.GetArrayElementAtIndex(i);
                IDisposable item = (IDisposable) BindingEditorUtils.GetPropertyTargetObject(property);
                item.Dispose();
            }
        }

        private void OnAddCallback(ReorderableList list)
        {
            beforeAdd?.Invoke();

            DisposeAll();

            if (list.index >= 0 && serializedProperty.arraySize > 0)
            {
                DuplicateAt(list.index);

                return;
            }

            serializedProperty.arraySize++;
        }

        private void OnRemoveCallback(ReorderableList list)
        {
            if (list.index < 0) return;

            beforeDelete?.Invoke(list.index);
            DisposeAll();

            if (list.serializedProperty != null)
            {
                list.serializedProperty.DeleteArrayElementAtIndex(list.index);
                if (list.index < list.serializedProperty.arraySize - 1)
                    return;
                list.index = list.serializedProperty.arraySize - 1;
            } else
            {
                list.list.RemoveAt(list.index);
                if (list.index >= list.list.Count - 1)
                    list.index = list.list.Count - 1;
            }
        }

        public T GetItemAt(int itemIndex)
        {
            if (itemIndex < 0 || itemIndex >= serializedProperty.arraySize) return default;

            return serializedProperty.GetArrayElementAtIndex(itemIndex).CastTo<T>();
        }

        public void SetItemAt(int itemIndex, T item)
        {
            if (itemIndex < 0 || itemIndex >= serializedProperty.arraySize) return;

            serializedProperty.CastTo<List<T>>()[itemIndex] = item;
        }

        public bool DuplicateAt(int itemIndex)
        {
            if (itemIndex < 0 || itemIndex >= serializedProperty.arraySize) return false;

            DisposeAll();

            var item = serializedProperty.GetArrayElementAtIndex(itemIndex);
            item.DuplicateCommand();

            return true;
        }

        private bool CopyAt(int itemIndex)
        {
            if (itemIndex < 0 || itemIndex >= serializedProperty.arraySize) return false;

            var item = GetItemAt(itemIndex);
            ClipboardUtils.Push(new PropertyList {items = new List<T> {item}});

            return true;
        }

        private bool CopyAll()
        {
            if (serializedProperty.arraySize == 0) return false;

            var propertyList = new PropertyList();
            for (var i = 0; i < serializedProperty.arraySize; i++)
            {
                propertyList.items.Add(GetItemAt(i));
            }

            ClipboardUtils.Push(propertyList);

            return true;
        }

        private bool PasteAt(int itemIndex)
        {
            var clipboardItems = ClipboardUtils.Get<PropertyList>();
            if (clipboardItems?.items == null || clipboardItems.items.Count == 0) return false;

            var startIndex = Mathf.Clamp(itemIndex, 0, serializedProperty.arraySize);
            var newIndex = startIndex;
            clipboardItems.items.ForEach(item =>
            {
                serializedProperty.InsertArrayElementAtIndex(newIndex);
                newIndex++;
            });

            serializedProperty.serializedObject.ApplyModifiedProperties();

            newIndex = startIndex;
            clipboardItems.items.ForEach(item =>
            {
                SetItemAt(newIndex, item);
                newIndex++;
            });
            serializedProperty.serializedObject.ApplyModifiedProperties();

            return true;
        }

        public void HandleEvents()
        {
            if (!Event.current.isKey || Event.current.type != EventType.KeyDown) return;

            if (index > 0 &&
                Event.current.keyCode == KeyCode.Delete
                || (Event.current.command && Event.current.shift && Event.current.keyCode == KeyCode.Backspace)
            )
            {
                Event.current.Use();

                OnRemoveCallback(this);
                return;
            }

            if (Event.current.keyCode == KeyCode.Plus
                || (Event.current.command && Event.current.keyCode == KeyCode.Return)
            )
            {
                Event.current.Use();

                OnAddCallback(this);
                return;
            }

#if UNITY_EDITOR_OSX
            if (!Event.current.command) return;
#else
            if (!Event.current.control) return;
#endif

            switch (Event.current.keyCode)
            {
                case KeyCode.C:
                {
                    if (Event.current.shift && CopyAll())
                    {
                        Event.current.Use();
                        break;
                    }

                    if (CopyAt(index))
                    {
                        Event.current.Use();
                    }

                    break;
                }
                case KeyCode.V:
                {
                    if (PasteAt(index + 1))
                    {
                        Event.current.Use();
                    }

                    break;
                }
                case KeyCode.D:
                {
                    if (DuplicateAt(index))
                    {
                        Event.current.Use();
                    }

                    break;
                }
            }
        }

        public new void DoLayoutList()
        {
            base.DoLayoutList();
            HandleEvents();
        }

        public new void DoList(Rect rect)
        {
            base.DoList(rect);
            HandleEvents();
        }
    }
}