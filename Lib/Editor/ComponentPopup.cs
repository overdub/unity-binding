using System;
using System.Collections.Generic;
using System.Linq;
using DataBinding.Utils;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Lib.Editor
{
    public static class ComponentPopup
    {
        private static List<Component> targetComponents = new List<Component>();
        private static List<string> componentPaths = new List<string>();
        private static List<string> componentAssemblyNames = new List<string>();

        public static void Draw(Rect rect, Transform root, SerializedProperty property, SerializedProperty assemblyNameProperty, string placeholder = "")
        {
            Draw(rect, null, root, property, assemblyNameProperty, placeholder);
        }

        public static void Draw(Rect rect, string label, Transform root, SerializedProperty property, SerializedProperty assemblyNameProperty, string placeholder = "")
        {
            if (root != null)
            {
                root.GetComponents(targetComponents);

                componentPaths = targetComponents.Where(component => component != null)
                    .Select(component =>
                    {
                        var componentType = component.GetType();
                        var name = componentType.FullName;
                        var group = targetComponents.Where(c => c.GetType() == componentType);
                        var groupSize = group.Count();

                        if (groupSize == 1)
                        {
                            return name;
                        }

                        return $"{name}[{group.ToList().FindIndex(c => c == component)}]";
                    })
                    .ToList();
                componentAssemblyNames = targetComponents.Where(component => component != null)
                    .Select(component =>
                    {
                        var componentType = component.GetType();
                        return componentType.Assembly.FullName;
                    })
                    .ToList();
            } else
            {
                targetComponents.Clear();
                componentPaths.Clear();
                componentAssemblyNames.Clear();
            }

            componentPaths.Insert(0, "GameObject");
            componentAssemblyNames.Insert(0, "");

            EditorGUI.BeginChangeCheck();
            EditablePopup.Draw(rect, label, property, componentPaths, placeholder);

            if (EditorGUI.EndChangeCheck())
            {
                var selectedIndex = componentPaths.IndexOf(property.stringValue);

                assemblyNameProperty.stringValue = selectedIndex == -1
                    ? EditorUtils.GetTypeFromName(property.stringValue)?.Assembly.FullName ?? ""
                    : componentAssemblyNames[selectedIndex];
                assemblyNameProperty.serializedObject.ApplyModifiedProperties();
            }
        }
    }
}