using System;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Lib.Editor
{
    [Serializable]
    public class ClipboardEntry
    {
        public string type;
        public string value;
    }

    public static class ClipboardUtils
    {
        private static void Push(string text)
        {
            EditorGUIUtility.systemCopyBuffer = text;
        }

        public static void Push<T>(T serializable)
        {
            Push(serializable.ToJson());
        }

        private static string Get()
        {
            return EditorGUIUtility.systemCopyBuffer;
        }

        public static T Get<T>() where T : class, new()
        {
            var buffer = Get();
            if (string.IsNullOrEmpty(buffer)) return default;

            return buffer.FromJson<T>();
        }
    }
}