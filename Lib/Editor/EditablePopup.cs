using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Lib.Editor
{
    public static class EditablePopup
    {
        private static GenericMenu popupMenu;
        private static int currentPopupControlId;
        private static string clickedOption = null;
        private static PopupEditorWindow popupWindow;
        
        public static void Draw(Rect position, SerializedProperty property, List<string> values, string placeholder = "")
        {
            Draw(position, GUIContent.none, property, values);
        }

        public static void Draw(Rect position, string label, SerializedProperty property, List<string> values, string placeholder = "")
        {
            Draw(position, new GUIContent(label), property, values, placeholder);
        }

        public static void Draw(Rect position, GUIContent label, SerializedProperty property, List<string> values, string placeholder = "")
        {
            var controlName = "tf" + position;
            GUI.SetNextControlName(controlName);

            var eventType = Event.current.type;
            var keyCode = Event.current.keyCode;
            
            property.stringValue = EditorGUI.TextField(position, label, property.stringValue);
            var controlId = EditorGUIReflectionUtils.LastControlId;

            if (! string.IsNullOrEmpty(placeholder) && property.stringValue == "")
            {
                GUI.color = new Color(1, 1, 1, 0.3f);
                var rect = new Rect(position);
                rect.x += 2;
                EditorGUI.LabelField(rect, placeholder);
                GUI.color = Color.white;
            }
            
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                var options = values.Where(v => String.Equals(v, property.stringValue, StringComparison.CurrentCultureIgnoreCase))
                    .Concat(values.Where(v => v.ToLower().Contains(property.stringValue.ToLower()) && v != property.stringValue)).ToList();

                if (options.Count == 1 && options[0] == property.stringValue)
                {
                    options.Clear();
                }
                
                if (clickedOption != null)
                {
                    SetPropertyValue(property, clickedOption);
                    clickedOption = null;
                }

                var rect = new Rect(position);
                if (label != GUIContent.none && label.text != "")
                {
                    rect.x += EditorGUIUtility.labelWidth + EditorGUIUtility.standardVerticalSpacing;
                    rect.width -= EditorGUIUtility.labelWidth + EditorGUIUtility.standardVerticalSpacing * 2;
                }
                rect.y += rect.height;

                if (popupWindow == null)
                {
                    popupWindow = ScriptableObject.CreateInstance<PopupEditorWindow>();
                    currentPopupControlId = controlId;
                    ShowPopupWindow(popupWindow);
                    popupWindow.property = property;
                    popupWindow.changed += value => { clickedOption = value; };
                    popupWindow.closed += () =>
                    {
                        popupWindow.Dispose();
                        popupWindow = null;
                    };
                }

                popupWindow.options = options;
                popupWindow.ProcessKeyboardInput(eventType, keyCode);
                popupWindow.AdjustSize(GUIUtility.GUIToScreenRect(rect));
            } else
            {
                if (popupWindow != null && currentPopupControlId == controlId)
                {
                    popupWindow.Close();
                    popupWindow.Dispose();
                    popupWindow = null;
                }
            }
        }

        private static void SetPropertyValue(SerializedProperty property, string value)
        {
            property.stringValue = value;
            property.serializedObject.ApplyModifiedProperties();
            GUI.changed = true;
        }

        private static void ShowPopupWindow(PopupEditorWindow popupWindow)
        {
            var showWithMode = typeof(EditorWindow).GetMethod("ShowPopupWithMode", BindingFlags.Instance | BindingFlags.NonPublic);
            showWithMode.Invoke(popupWindow, new object[] {1, false});
        }
    }

    [DefaultExecutionOrder(-10000)]
    internal class PopupEditorWindow : EditorWindow
    {
        public SerializedProperty property;
        public List<string> options;
        public int selectedIndex;

        public event Action<string> changed;
        public event Action closed;

        private int lastSelectedIndex;
        private GUIStyle buttonStyle;
        private GUIStyle selectedButtonStyle;
        private Vector2 scrollPosition = Vector2.zero;
        private float lineHeight;

        private static PopupEditorWindow instance;

        private void Awake()
        {
            instance = this;
        }

        private void OnDestroy()
        {
            Dispose();
        }
        
        private Event evt = new Event();

        public bool PreprocessKeyboardInput()
        {
            if (Event.current.type == EventType.KeyDown || Event.current.type == EventType.KeyUp)
            {
                switch (Event.current.keyCode)
                {
                    case KeyCode.UpArrow:
                        Event.current.Use();
                        return true;
                    case KeyCode.DownArrow:
                        Event.current.Use();
                        return true;
                    case KeyCode.Return:
                        Event.current.Use();
                        return true;
                }
            }
            return false;
        }
        
        public void ProcessKeyboardInput(EventType type, KeyCode keyCode)
        {
            if (type == EventType.KeyDown)
            {
                switch (keyCode)
                {
                    case KeyCode.UpArrow:
                        selectedIndex = Mathf.Max(selectedIndex - 1, 0);
                        Repaint();
                        break;
                    case KeyCode.DownArrow:
                        selectedIndex = Mathf.Min(selectedIndex + 1, options.Count() - 1);
                        Repaint();
                        break;
                    case KeyCode.Return:
                        if (selectedIndex < options.Count())
                        {
                            changed?.Invoke(options[selectedIndex]);
                        }
                        Repaint();
                        break;
                    default:
                        selectedIndex = 0;
                        break;
                }
            }
        }


        private void OnGUI()
        {
            if (instance == null)
            {
                Close();
                Dispose();
                DestroyImmediate(this);
                return;
            }

            try
            {
                if (property.serializedObject == null || property.propertyPath == "")
                {
                    DoClose();
                }
            } catch (Exception)
            {
                Close();
                Dispose();
                DestroyImmediate(this);
                return;
            }
  
            InitStyles();

            if (selectedIndex != lastSelectedIndex)
            {
                lastSelectedIndex = selectedIndex;

                // scroll selected into view
                if (selectedIndex * lineHeight < scrollPosition.y)
                {
                    scrollPosition.y = selectedIndex * lineHeight;
                }

                if ((selectedIndex + 1) * lineHeight > scrollPosition.y + position.height)
                {
                    scrollPosition.y = (selectedIndex + 1) * lineHeight - position.height;
                }
            }

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUIStyle.none, GUI.skin.verticalScrollbar);
            for (var i = 0; i < options.Count; i++)
            {
                var o = options[i];

                if (GUILayout.Button(o, (i == selectedIndex) ? selectedButtonStyle : buttonStyle))
                {
                    selectedIndex = i;
                    changed?.Invoke(o);
                    DoClose();
                }
            }

            GUILayout.EndScrollView();
        }

        private void InitStyles()
        {
            if (buttonStyle != null) return;

            buttonStyle = new GUIStyle(EditorStyles.label)
            {
                padding = new RectOffset(8, 8, 2, 2),
                margin = new RectOffset(0, 0, 0, 0),
                fontSize = EditorStyles.label.fontSize - 1
            };

            var bgColor = new Color(0.22f, 0.52f, 0.96f);
            var bgTexture = new Texture2D(2, 2);
            bgTexture.SetPixels(new[]
            {
                bgColor, bgColor, bgColor, bgColor
            });
            bgTexture.Apply();

            selectedButtonStyle = new GUIStyle(buttonStyle)
            {
                normal =
                {
                    background = bgTexture,
                    textColor = Color.white
                }
            };

            lineHeight = buttonStyle.CalcHeight(new GUIContent("A"), position.width);
        }

        public void AdjustSize(Rect rect)
        {
            InitStyles();

            var longestOption = "";
            options.ForEach(o =>
            {
                if (o.Length > longestOption.Length)
                {
                    longestOption = o;
                }
            });
            var optionWidth = buttonStyle.CalcSize(new GUIContent(longestOption)).x;

            rect.width = Mathf.Max(optionWidth + 8, rect.width);
            rect.height = Mathf.Min(options.Count(), 4) * lineHeight;
            position = rect;
        }

        private void DoClose()
        {
            Close();
            closed?.Invoke();
        }

        public void Dispose()
        {
            property = null;
            changed = null;
            closed = null;
        }
    }
}