// Decompiled with JetBrains decompiler
// Type: DG.DemiEditor.SerializedPropertyExtensions
// Assembly: DemiEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9300573E-E072-4493-B712-9BF31379F58F
// Assembly location: /Users/frank/Documents/PRJ/TapeHeadGames/TweenMachine/Assets/Demigiant/DemiLib/Core/Editor/DemiEditor.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace DataBinding.Lib.Editor
{
  public static class SerializedPropertyExtensions
  {
    /// <summary>
    /// Returns the value of the given property (works like a cast to type).
    /// <para>
    /// Improved from HiddenMonk's functions (http://answers.unity3d.com/questions/627090/convert-serializedproperty-to-custom-class.html)
    /// </para>
    /// </summary>
    public static T CastTo<T>(this SerializedProperty property)
    {
      object obj = (object) property.serializedObject.targetObject;
      string[] strArray = property.propertyPath.Split('.');
      int length = strArray.Length;
      for (int index = 0; index < length; ++index)
      {
        string fieldName = strArray[index];
        if (fieldName == "Array")
        {
          if (index < length - 2)
          {
            string str1 = strArray[index + 1];
            string str2 = str1.Substring(str1.IndexOf("[") + 1);
            string str3 = str2.Substring(0, str2.Length - 1);
            obj = ((IList) obj)[Convert.ToInt32(str3)];
            ++index;
          }
          else
          {
            int indexInArray = property.GetIndexInArray();
            IList<T> objList = (IList<T>) obj;
            if (objList.Count - 1 >= indexInArray)
              return objList[indexInArray];
            return default (T);
          }
        }
        else
          obj = SerializedPropertyExtensions.GetFieldOrPropertyValue<object>(fieldName, obj);
      }
      return (T) obj;
    }

    /// <summary>Returns TRUE if this property is inside an array</summary>
    public static bool IsArrayElement(this SerializedProperty property)
    {
      return property.propertyPath.Contains("Array");
    }

    /// <summary>
    /// Returns -1 if the property is not inside an array, otherwise returns its index inside the array
    /// </summary>
    public static int GetIndexInArray(this SerializedProperty property)
    {
      if (!property.IsArrayElement())
        return -1;
      int startIndex = property.propertyPath.LastIndexOf('[') + 1;
      int length = property.propertyPath.LastIndexOf(']') - startIndex;
      return int.Parse(property.propertyPath.Substring(startIndex, length));
    }

    private static T GetFieldOrPropertyValue<T>(string fieldName, object obj)
    {
      System.Type type = obj.GetType();
      System.Reflection.FieldInfo field = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
      if ((object) field != null)
        return (T) field.GetValue(obj);
      PropertyInfo property = type.GetProperty(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
      if ((object) property != null)
        return (T) property.GetValue(obj, (object[]) null);
      return default (T);
    }

//    public static void CopySerializedProperty();
  }
}
