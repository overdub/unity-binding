using UnityEngine;

namespace DataBinding.Lib
{
    public static class EditorUtils
    {
        public static void SetDirty(Object target)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying && target != null)
            {
                UnityEditor.EditorUtility.SetDirty(target);
            }
#endif
        }
        
        public static void Destroy(Object o)
        {
            if (o == null) return;
            
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                if (UnityEditor.Selection.activeObject == o)
                {
                    UnityEditor.Selection.activeObject = null;
                }
                Object.DestroyImmediate(o);
                return;
            }
#endif

            Object.Destroy(o);
        }
    }
}