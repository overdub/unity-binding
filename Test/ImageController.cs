﻿using DataBinding.Core.Attributes;
using DataBinding.Properties;
using UnityEngine;
using UnityEngine.UI;

namespace DataBinding.Test
{
    public class ImageController : Controller
    {
        private Image image;
        [Binding(nameof(TestModel.Color)), SerializeField] private BoundProperty<Color> color;
        
        protected override void OnEnable()
        {
            image = GetComponent<Image>();
            base.OnEnable();
        }

        [BindingChangeListener(nameof(TestModel.Color))] private void OnColorChanged()
        {
            image.color = color.Value;
        }
    }
}
