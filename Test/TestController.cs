using DataBinding.Core.Attributes;
using DataBinding.Properties;
using UnityEngine;

namespace DataBinding.Test
{
    public class TestController : Controller
    {
        [Binding(nameof(TestModel.ImageEnabled)), SerializeField] private BoundProperty<bool> imageEnabled;

        [BindingChangeListener(nameof(TestModel.ImageEnabled))] private void ImageEnabledChanged()
        {
            Debug.Log(nameof(ImageEnabledChanged));
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }
    }
}