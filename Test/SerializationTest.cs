using System;
using DataBinding.Lib;
using NaughtyAttributes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Test
{
    [Serializable]
    public class TestData
    {
        [SerializeField] private Object objectReference;
    }
    
    public class SerializationTest : MonoBehaviour
    {
        [SerializeField, Header("Serializable")] private TestData testData;
        [SerializeField, Header("Json")] private string json;

        [Button(nameof(Clear))]
        private void Clear()
        {
            testData = new TestData();
            EditorUtils.SetDirty(this);
        }
        
        [Button(nameof(Serialize))]
        private void Serialize()
        {
            json = testData.ToJson();
            Debug.Log(json);
            EditorUtils.SetDirty(this);
        }

        [Button(nameof(Deserialize))]
        private void Deserialize()
        {
            testData = json.FromJson<TestData>();
            Debug.Log(testData);
            EditorUtils.SetDirty(this);
        }
    }
}