using UnityEngine;

namespace DataBinding.Test
{
    [ExecuteAlways]
    public class SequenceTestModel : Model
    {
        public enum StateEnum
        {
            Off, 
            On,
        }
        
        [SerializeField] private StateEnum toggle;

        public StateEnum Toggle
        {
            get => toggle;
            set => SetPropertyValue(ref toggle, value);
        }
    }
}