using UnityEngine;
using UnityEngine.UI;

namespace DataBinding.Test
{
    [ExecuteAlways]
    public class TestModel : GlobalModel
    {
        [SerializeField] private bool imageEnabled;

        public bool ImageEnabled
        {
            get => imageEnabled;
            set => SetPropertyValue(ref imageEnabled, value);
        }

        [SerializeField, Range(0, 1)] private float fillAmount;
        
        public float FillAmount
        {
            get => fillAmount;
            set => SetPropertyValue(ref fillAmount, value);
        }

        [SerializeField] private Vector3 position;
        
        public Vector3 Position
        {
            get => position;
            set => SetPropertyValue(ref position, value);
        }

        [SerializeField] private Color color;
        
        public Color Color
        {
            get => color;
            set => SetPropertyValue(ref color, value);
        }

        [SerializeField] private int intValue;
        
        public int IntValue
        {
            get => intValue;
            set => SetPropertyValue(ref intValue, value);
        }

        [SerializeField] private string stringValue;

        public string StringValue
        {
            get => stringValue;
            set => SetPropertyValue(ref stringValue, value);
        }

        [SerializeField] private Transform childTransform;

        public Transform ChildTransform
        {
            get => childTransform;
            set => SetPropertyValue(ref childTransform, value);
        }

        [SerializeField] private Image imageReference;

        public Image ImageReference
        {
            get => imageReference;
            set => SetPropertyValue(ref imageReference, value);
        }
    }
}