using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace DataBinding.Test
{
    public class StressTest : MonoBehaviour
    {
        [SerializeField] private GameObject instancePrefab;
        [SerializeField] private int instanceCount = 1000;
        [SerializeField] private bool noReflection = false;

        private List<Image> images = new List<Image>();
        private TestModel model;

        private void OnEnable()
        {
            model = GetComponent<TestModel>();
            
            Clear();

            for (var i = 0; i < instanceCount; i++)
            {
                var instance = Instantiate(instancePrefab, transform);
                (instance.transform as RectTransform).anchoredPosition = Random.insideUnitCircle * (Screen.width * 0.5f);
                (instance.transform as RectTransform).localScale = new Vector3(1, 1, 1) * Random.Range(0.5f, 1.5f);
                
                images.Add(instance.GetComponent<Image>());
            }
        }

        private void Clear()
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(0));
            }

            images.Clear();
        }

        private void Update()
        {
            var color = Color.HSVToRGB((Time.time * 0.25f) % 1f, 0.6f, 0.6f);
            
            if (noReflection)
            {
                images.ForEach(image =>
                {
                    image.fillAmount = Mathf.Sin(Time.time) * 0.5f + 0.5f;
                    image.color = color;
                });
                return;
            }
            
            model.FillAmount = Mathf.Sin(Time.time) * 0.5f + 0.5f;
            model.Color = color;
        }

        private void OnDisable()
        {
            Clear();
        }
    }
}