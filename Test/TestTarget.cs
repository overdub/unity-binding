using UnityEngine;

namespace DataBinding.Test
{
    [ExecuteAlways]
    public class TestTarget : MonoBehaviour
    {
        [SerializeField] private bool testBool;

        [SerializeField] private int _intProperty;
        public int IntProperty
        {
            get => _intProperty;
            set => _intProperty = value;
        }

        public float publicFloat;
    }
}