using UnityEngine;

namespace DataBinding.Test
{
    [ExecuteAlways]
    public class TestChildModel : Model
    {
        [SerializeField] private bool boolValue;

        public bool BoolValue
        {
            get => boolValue;
            set => SetPropertyValue(ref boolValue, value);
        }
    }
    
}