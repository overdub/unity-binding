using System;
using DataBinding.Properties;
using NaughtyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DataBinding.Test
{
    [ExecuteAlways]
    public class MemberAccessorTest : MonoBehaviour
    {
        [SerializeField] private DynamicModelProperty modelProperty;
        
        public int intValue;
        [SerializeField] private DynamicMemberAccessor dynamicAccessor = new DynamicMemberAccessor(typeof(int));
        
        private Action<int> dynamicAccessorSetter;

        [Button("Write random value")]
        private void WriteRandomValue()
        {
            dynamicAccessorSetter(Random.Range(0, 100));
        }


        private void OnEnable()
        {
            dynamicAccessor.reconnected += OnReconnected;
            OnReconnected();
        }

        private void OnReconnected()
        {
            dynamicAccessorSetter = dynamicAccessor.GetTypedSetter<int>();
        }

        private void OnDisable()
        {
            dynamicAccessor.reconnected -= OnReconnected;
        }
    }
}