using System;
using System.Collections.Generic;
using System.Reflection;
using DataBinding.Core;

namespace DataBinding
{
    public interface IModel : IWatchable
    {
        List<string> PropertyNames { get; }
        bool Has(string propertyName);
        Type GetPropertyType(string propertyName);
        object Get(string property);
        PropertyInfo GetPropertyInfo(string property);
        object GetPropertyTarget(string property);
    }
}