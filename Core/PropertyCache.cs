using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Utils;
using UnityEngine;

namespace DataBinding.Core
{
    public class PropertyCache
    {
        private struct PropertyDescription
        {
            public object target;
            public PropertyInfo propertyInfo;
        }

        public List<string> PropertyNames { get; private set; } = new List<string>();
        public int PropertyCount => properties.Count;

        private readonly Dictionary<string, PropertyDescription> properties = new Dictionary<string, PropertyDescription>();
        private readonly Dictionary<string, string> propertyNamesByFieldNames = new Dictionary<string, string>();

        private readonly object target;

        public PropertyCache(object target)
        {
            this.target = target;

            Recache();
        }

        private void AddProperties(object target, Dictionary<string, PropertyDescription> properties)
        {
            if (target == null) return;

            BindingReflectionUtils.GetAllSerializedFields(target.GetType()).ToList().ForEach(field =>
            {
                var propertyName = field.Name.StartsWith("m_") ? field.Name.Substring(2) : (field.Name.StartsWith("_") ? field.Name.Substring(1) : field.Name);
                propertyName = propertyName.Substring(0, 1).ToUpper() + propertyName.Substring(1);

                var propertyInfo = target.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo == null) return;
                
                properties[propertyName] = new PropertyDescription
                {
                    target = target,
                    propertyInfo = propertyInfo,
                };
                propertyNamesByFieldNames.Add(field.Name, propertyName);
            });
        }

        private void Recache()
        {
            properties.Clear();
            propertyNamesByFieldNames.Clear();

            AddProperties(target, properties);

            PropertyNames = properties.Keys.ToList();
        }

        public bool Has(string propertyName)
        {
            return PropertyNames.Contains(propertyName);
        }

        public Type GetPropertyType(string propertyName)
        {
            return properties.ContainsKey(propertyName) ? properties[propertyName].propertyInfo.PropertyType : null;
        }

        public PropertyInfo GetPropertyInfo(string property)
        {
            return !properties.ContainsKey(property) ? null : properties[property].propertyInfo;
        }

        public string FindPropertyByFieldName(string serializedFieldName)
        {
            return propertyNamesByFieldNames.ContainsKey(serializedFieldName) ? propertyNamesByFieldNames[serializedFieldName] : null;
        }

        internal object GetPropertyTarget(string propertyName)
        {
            return !properties.ContainsKey(propertyName) ? null : properties[propertyName].target;
        }
        
        public void Set(string propertyName, object value)
        {
            if (!properties.ContainsKey(propertyName))
            {
                Debug.LogWarning($"Property {propertyName} not found.");
                return;
            }

            var property = properties[propertyName];
            if (property.propertyInfo.CanWrite)
            {
                property.propertyInfo.SetValue(property.target, value);
            }
        }

        public object Get(string propertyName)
        {
            if (!properties.ContainsKey(propertyName))
            {
                Debug.LogWarning($"Property {propertyName} not found.");
                return null;
            }

            var property = properties[propertyName];
            return property.propertyInfo.GetValue(property.target);
        }
    }
}