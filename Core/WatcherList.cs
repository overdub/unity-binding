using System;
using System.Collections.Generic;

namespace DataBinding.Core
{
    public class WatcherList
    {
        private bool listenersLocked;

        private bool ListenersLocked
        {
            get => listenersLocked;
            set
            {
                listenersLocked = value;
                if (!listenersLocked)
                {
                    while (actionQueue.Count > 0)
                    {
                        actionQueue.Dequeue()?.Invoke();
                    }
                }
            }
        }

        private Dictionary<string, Dictionary<Action, Action>> listeners = new Dictionary<string, Dictionary<Action, Action>>();

        private Queue<Action> actionQueue = new Queue<Action>();

        public void Dispatch(string propertyName)
        {
            if (!listeners.ContainsKey(propertyName)) return;

            if (listenersLocked)
            {
                actionQueue.Enqueue(() => Dispatch(propertyName));
                return;
            }

            ListenersLocked = true;
            foreach (var data in listeners[propertyName])
            {
                data.Value.Invoke();
            }
            ListenersLocked = false;
        }

        public void Watch(string property, Action listener, Action wrapper)
        {
            if (listenersLocked)
            {
                actionQueue.Enqueue(() => Watch(property, listener, wrapper));
                return;
            }

            if (!listeners.ContainsKey(property))
            {
                listeners.Add(property, new Dictionary<Action, Action>());
            }

            listeners[property].Add(listener, wrapper);
        }

        public void Watch(string property, Action listener)
        {
            if (listenersLocked)
            {
                actionQueue.Enqueue(() => Watch(property, listener));
                return;
            }

            Watch(property, listener, listener);
        }

        public void Unwatch(string property, Action listener)
        {
            if (listenersLocked)
            {
                actionQueue.Enqueue(() => Unwatch(property, listener));
                return;
            }

            if (listeners.ContainsKey(property))
            {
                listeners[property].Remove(listener);
            }
        }

        public void UnwatchAll()
        {
            listeners.Clear();
        }

        public void UnwatchAll(string property)
        {
            if (listeners.ContainsKey(property))
            {
                listeners.Remove(property);
            }
        }
    }
}