using System;
using System.Reflection;
using DataBinding.Core;
using DataBinding.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;

namespace DataBinding.Properties
{
    public class PropertyTargetConnector<TS, TD> : IPropertyTargetConnector
    {
        private Func<TS> getter;
        private Action<TS> setter;

        private object target;

        public PropertyTargetConnector(object target, Func<TS> getter, Action<TS> setter)
        {
            this.target = target;
            this.getter = getter;
            this.setter = setter;
        }

        public void Apply()
        {
            setter(getter());
            
#if UNITY_EDITOR
            if (!Application.isPlaying && target is Object unityObject)
            {
                EditorUtility.SetDirty(unityObject);
            }
#endif
        }
    }
    
    public static class PropertyTargetConnector
    {
    // TODO: Object => object?
        public static IPropertyTargetConnector Connect(object source, MemberInfo sourceInfo, object target, MemberInfo targetInfo)
        {
            var fromType = BindingReflectionUtils.GetMemberOutputType(sourceInfo);
            var toType = BindingReflectionUtils.GetMemberInputType(targetInfo);
            
            if (fromType == typeof(float) && toType == typeof(float))
            {
                return new PropertyTargetConnector<float, float>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<float>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<float>(target, targetInfo)
                );
            }

            if (fromType == typeof(int) && toType == typeof(int))
            {
                return new PropertyTargetConnector<int, int>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<int>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<int>(target, targetInfo)
                );
            }

            if (fromType == typeof(string) && toType == typeof(string))
            {
                return new PropertyTargetConnector<string, string>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<string>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<string>(target, targetInfo)
                );
            }

            if (fromType == typeof(bool))
            {
                if (toType == typeof(bool))
                {
                    return new PropertyTargetConnector<bool, bool>(
                        target,
                        BindingReflectionUtils.CreateTypedGetter<bool>(source, sourceInfo),
                        BindingReflectionUtils.CreateTypedSetter<bool>(target, targetInfo)
                    );
                }

                if (toType == typeof(int))
                {
                    return new PropertyTargetConnector<bool, int>(
                        target,
                        BindingReflectionUtils.CreateTypedGetter<bool>(source, sourceInfo),
                        BindingReflectionUtils.CreateTypedConversionSetter<bool, int>(target, targetInfo, value => value ? 1 : 0)
                    );
                }
            }

            if (fromType == typeof(Vector2) && toType == typeof(Vector2))
            {
                return new PropertyTargetConnector<Vector2, Vector2>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<Vector2>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<Vector2>(target, targetInfo)
                );
            }

            if (fromType == typeof(Vector3) && toType == typeof(Vector3))
            {
                return new PropertyTargetConnector<Vector3, Vector3>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<Vector3>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<Vector3>(target, targetInfo)
                );
            }

            if (fromType == typeof(Quaternion) && toType == typeof(Quaternion))
            {
                return new PropertyTargetConnector<Quaternion, Quaternion>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<Quaternion>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<Quaternion>(target, targetInfo)
                );
            }

            if (fromType == typeof(Color) && toType == typeof(Color))
            {
                return new PropertyTargetConnector<Color, Color>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<Color>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<Color>(target, targetInfo)
                );
            }

            if (fromType == typeof(Color32) && toType == typeof(Color32))
            {
                return new PropertyTargetConnector<Color32, Color32>(
                    target,
                    BindingReflectionUtils.CreateTypedGetter<Color32>(source, sourceInfo),
                    BindingReflectionUtils.CreateTypedSetter<Color32>(target, targetInfo)
                );
            }

            return new PropertyTargetConnector<object, object>(
                target,
                BindingReflectionUtils.CreateUntypedGetter(source, sourceInfo),
                targetInfo != null ? BindingReflectionUtils.CreateUntypedSetter(fromType, target, targetInfo) : value => { }
            );
        }
    }
}