using System;
using System.Collections.Generic;

namespace DataBinding.Core
{
    public interface IWatchable
    {
        void Watch(string property, Action listener);
        void Watch(string property, Action listener, Action wrapper, bool initialDispatch);
        void Unwatch(string property, Action listener);
        
    }
}