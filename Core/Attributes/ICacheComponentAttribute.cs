using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    public interface ICacheComponentAttribute
    {
        void TryInitialize(object obj, FieldInfo info, Transform transform);
    }
}