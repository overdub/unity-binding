using System;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentInChildrenAttribute : Attribute, ICacheComponentAttribute
    {
        public bool includeInactive = true;

        public GetComponentInChildrenAttribute(bool includeInactive)
        {
            this.includeInactive = includeInactive;
        }

        public GetComponentInChildrenAttribute()
        {
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! typeof(Component).IsAssignableFrom(info.FieldType))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Component");
            }

            info.SetValue(obj, transform.GetComponentInChildren(info.FieldType, includeInactive));
        }
    }
}