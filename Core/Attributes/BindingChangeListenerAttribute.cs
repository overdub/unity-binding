using System;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class BindingChangeListenerAttribute : Attribute
    {
        public string propertyName;
        
        public BindingChangeListenerAttribute(string propertyName)
        {
            this.propertyName = propertyName;
        }
    }
}