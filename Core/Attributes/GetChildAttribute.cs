using System;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GetChildAttribute : Attribute, ICacheComponentAttribute
    {
        public uint index;

        public GetChildAttribute(uint index)
        {
            this.index = index;
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! typeof(Component).IsAssignableFrom(info.FieldType))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Component");
            }

            info.SetValue(obj, index < transform.childCount ? transform.GetChild((int) index) : null);
        }
    }
}