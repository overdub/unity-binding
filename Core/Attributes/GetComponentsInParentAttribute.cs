using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentsInParentAttribute : Attribute, ICacheComponentAttribute
    {
        public bool includeInactive = true;

        public GetComponentsInParentAttribute(bool includeInactive)
        {
            this.includeInactive = includeInactive;
        }

        public GetComponentsInParentAttribute()
        {
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! info.FieldType.IsArray || ! typeof(Component).IsAssignableFrom(info.FieldType.GetElementType()))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Component[]");
            }

            var parents = transform.GetComponentsInParent(info.FieldType.GetElementType(), includeInactive);
            var elementType = info.FieldType.GetElementType();
            var convertedparents = Array.CreateInstance(elementType, parents.Length);
            Array.Copy(parents, convertedparents, parents.Length);
            
            info.SetValue(obj, convertedparents);
        }
    }
}