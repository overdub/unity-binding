using System;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class FindAttribute : Attribute, ICacheComponentAttribute
    {
        public string path;

        public FindAttribute(string path)
        {
            this.path = path;
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! typeof(Transform).IsAssignableFrom(info.FieldType))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Transform");
            }

            info.SetValue(obj, transform.Find(path));
        }
    }
}