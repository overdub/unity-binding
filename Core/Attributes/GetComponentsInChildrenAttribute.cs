using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentsInChildrenAttribute : Attribute, ICacheComponentAttribute
    {
        public bool includeInactive = true;

        public GetComponentsInChildrenAttribute(bool includeInactive)
        {
            this.includeInactive = includeInactive;
        }

        public GetComponentsInChildrenAttribute()
        {
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! info.FieldType.IsArray || ! typeof(Component).IsAssignableFrom(info.FieldType.GetElementType()))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Component[]");
            }

            var children = transform.GetComponentsInChildren(info.FieldType.GetElementType(), includeInactive);
            var elementType = info.FieldType.GetElementType();
            var convertedChildren = Array.CreateInstance(elementType, children.Length);
            Array.Copy(children, convertedChildren, children.Length);
            
            info.SetValue(obj, convertedChildren);
        }
    }
}