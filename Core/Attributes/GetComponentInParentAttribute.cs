using System;
using System.Reflection;
using UnityEngine;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GetComponentInParentAttribute : Attribute, ICacheComponentAttribute
    {
        public GetComponentInParentAttribute()
        {
        }
        
        public void TryInitialize(object obj, FieldInfo info, Transform transform)
        {
            if (! typeof(Component).IsAssignableFrom(info.FieldType))
            {
                throw new InvalidCastException("Field type has to extend UnityEngine.Component");
            }

            info.SetValue(obj, transform.GetComponentInParent(info.FieldType));
        }
    }
}