using System;

namespace DataBinding.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BindingAttribute : Attribute
    {
        public string propertyName;
        public string changeListener;
        
        public BindingAttribute(string propertyName, string changeListener = "")
        {
            this.propertyName = propertyName;
            this.changeListener = changeListener;
        }

        public BindingAttribute()
        {
        }
    }
}