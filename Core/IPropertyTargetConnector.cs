namespace DataBinding.Core
{
    public interface IPropertyTargetConnector
    {
        void Apply();
    }
}