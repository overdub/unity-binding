using System;
using System.Collections.Generic;
using UnityEngine;

namespace DataBinding
{
    [ExecuteAlways, DefaultExecutionOrder(-100)]
    public class GlobalModel : Model
    {
        private static List<IModel> instances = new List<IModel>();

        public static List<IModel> Instances => instances;

        public static IModel GetModel(string propertyName)
        {
            foreach (var model in instances)
            {
                if (model != null && model.Has(propertyName))
                {
                    return model;
                }
            }

            return null;
        }

        protected virtual void OnEnable()
        {
            if (instances == null)
            {
                instances = new List<IModel>();
            }

            var existing = instances.Find(instance => instance.GetType() == GetType());
            
            if (existing != null && existing != this)
            {
                if (!Application.isPlaying)
                {
                    throw new ArgumentException($"Model of type {GetType().Name} already exists.");
                }
                
                Destroy(this);
                
                return;
            }

            if (Application.isPlaying)
            {
                DontDestroyOnLoad(this);
            }
            instances.Add(this);
        }
    }
}