using System;
using System.Linq;
using System.Reflection;
using DataBinding.Core;
using DataBinding.Properties;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding
{
    [ExecuteAlways]
    public class DataBindingDispatcher : MonoBehaviour
    {
        [SerializeField] private DynamicModelProperty modelProperty;
        [SerializeField] private string displayedType;

        private IPropertyTargetConnector connector;

        public bool boolValue;
        public float floatValue;
        public int intValue;
        public Vector2 vector2Value;
        public Vector3 vector3Value;
        public Quaternion quaternionValue;
        public Color colorValue;
        public Color32 color32Value;
        public Transform transformValue;
        public Object objectValue;

        public string PropertyName
        {
            get => modelProperty.PropertyName;
            set { modelProperty.PropertyName = value; }
        }

        public void Connect()
        {
            modelProperty.Connect(transform);

            if (!modelProperty.Connected) return;

            var fieldInfo = GetValueFieldInfo(modelProperty.PropertyType);
            if (fieldInfo == null) return;

            connector = PropertyTargetConnector.Connect(this, fieldInfo, modelProperty.Model, modelProperty.Model.GetPropertyInfo(modelProperty.PropertyName));
        }

        public MemberInfo GetValueFieldInfo(Type propertyType)
        {
            var fieldInfo = GetType().GetFields(BindingFlags.Instance | BindingFlags.Public)
                .FirstOrDefault(info => info.FieldType == modelProperty.PropertyType && info.Name.EndsWith("Value"));

            if (fieldInfo != null) return fieldInfo;
            
            if (BindingReflectionUtils.ExtendsType(propertyType, typeof(Object)))
            {
                return GetType().GetField(nameof(objectValue), BindingFlags.Instance | BindingFlags.Public);
            }

            return null;
        }

        private void OnEnable()
        {
            Connect();
        }

        private void OnTransformParentChanged()
        {
            Connect();
        }

        public void Execute()
        {
            if (!modelProperty.Connected)
            {
                Connect();
            }

            connector?.Apply();
        }
    }
}