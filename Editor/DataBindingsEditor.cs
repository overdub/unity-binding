using DataBinding.Lib.Editor;
using DataBinding.Utils;
using UnityEditor;
using UnityEngine;

namespace DataBinding
{
    [CustomEditor(typeof(DataBindings))]
    public class DataBindingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (target == null) return;
            
            base.OnInspectorGUI();

            if (EditorApplication.isCompiling) return;
            
            var generatedControllerProperty = serializedObject.FindProperty("generatedController");
            if (generatedControllerProperty.stringValue == "") return;
            
            serializedObject.ApplyModifiedProperties();
            
            var controllerType = EditorUtils.GetTypeFromName(generatedControllerProperty.stringValue);
            generatedControllerProperty.stringValue = "";
            serializedObject.ApplyModifiedProperties();
            if (controllerType == null) return;

            var gameObject = (target as Component).gameObject;
            Undo.RecordObject(gameObject, "Undo generate controller");
            if (gameObject.GetComponent(controllerType) == null)
            {
                Undo.AddComponent(gameObject, controllerType); 
                EditorUtility.SetDirty(gameObject);
            }

            EditorApplication.update += DestroyDelayed;
        }

        private void DestroyDelayed()
        {
            EditorApplication.update -= DestroyDelayed;
            Undo.DestroyObjectImmediate(target);
        }

        private void OnEnable()
        {
            EditorApplication.contextualPropertyMenu += OnContextualPropertyMenu;
        }

        private void OnDisable()
        {
            EditorApplication.contextualPropertyMenu -= OnContextualPropertyMenu;
        }

        private void AddBindingMenuItem(GenericMenu menu, SerializedProperty property, string title, string propertyName)
        {
            menu.AddItem(new GUIContent(title), false, () =>
            {
                var dataBindings = target as DataBindings;
                dataBindings.Add(property.serializedObject.targetObject, propertyName);
            });   
        }

        private void OnContextualPropertyMenu(GenericMenu menu, SerializedProperty property)
        {
            var targetObject = property.serializedObject.targetObject;
            
            if (targetObject is RectTransform && property.propertyPath.Contains("Position"))
            {
                AddBindingMenuItem(menu, property, "Bind anchoredPosition3D", "anchoredPosition3D");
                AddBindingMenuItem(menu, property, "Bind anchoredPosition", "anchoredPosition");
                return;
            }
            if (targetObject is Transform && property.propertyPath.Contains("Position"))
            {
                AddBindingMenuItem(menu, property, "Bind localPosition", "localPosition");
                AddBindingMenuItem(menu, property, "Bind position", "position");
                return;
            }
            if (targetObject is Transform && property.propertyPath.Contains("Rotation"))
            {
                AddBindingMenuItem(menu, property, "Bind localRotation", "localRotation");
                AddBindingMenuItem(menu, property, "Bind rotation", "rotation");
                return;
            }
            
            var memberInfo = BindingReflectionUtils.FindWritableMember(targetObject.GetType(), property.propertyPath);
                
            if (memberInfo == null) return;

            AddBindingMenuItem(menu, property, "Bind " + memberInfo.Name, memberInfo.Name);
        }
    }
}