using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using DataBinding.Properties;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding
{
    [CustomEditor(typeof(DataBindingDispatcher))]
    public class DataBindingDispatcherEditor : Editor
    {
        private string[] dispatcherTypeNames;
        private List<string> dispatcherTypeValues;
        private string[] dispatcherValueFieldNames;

        public override void OnInspectorGUI()
        {
            var dataBindingDispatcher = target as DataBindingDispatcher;
            var modelPropertyProperty = serializedObject.FindProperty("modelProperty");
            var modelProperty = BindingEditorUtils.GetPropertyTargetObject(modelPropertyProperty) as DynamicModelProperty;

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(modelPropertyProperty);

            if (modelProperty.Connected)
            {
                var fieldInfo = dataBindingDispatcher.GetValueFieldInfo(modelProperty.PropertyType);

                if (fieldInfo != null)
                {
                    var valueProperty = serializedObject.FindProperty(fieldInfo.Name);
                    EditorGUILayout.PropertyField(valueProperty);
                }
            } else
            {
                var displayedTypeProperty = serializedObject.FindProperty("displayedType");
                var newIndex = EditorGUILayout.Popup(displayedTypeProperty.displayName, dispatcherTypeValues.IndexOf(displayedTypeProperty.stringValue), dispatcherTypeNames);
                newIndex = Mathf.Clamp(newIndex, 0, dispatcherTypeValues.Count - 1);
                displayedTypeProperty.stringValue = dispatcherTypeValues[newIndex];

                var valueProperty = serializedObject.FindProperty(dispatcherValueFieldNames[newIndex]);
                EditorGUILayout.PropertyField(valueProperty);
            }

            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();

                dataBindingDispatcher.Connect();
            }

            if (GUILayout.Button("Execute"))
            {
                dataBindingDispatcher.Execute();
            }
        }

        private void OnEnable()
        {
            var fields = target.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public)
                .Where(info => info.Name.EndsWith("Value"));
            dispatcherValueFieldNames = fields.Select(info => info.Name).ToArray();
            dispatcherTypeValues = fields.Select(info => info.FieldType.Name).ToList();
            dispatcherTypeNames = fields.Select(info => BindingReflectionUtils.GetTypeAlias(info.FieldType)).ToArray();
        }
    }
}