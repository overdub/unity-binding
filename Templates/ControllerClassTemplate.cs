/*<Using>*/

using DataBinding;
using DataBinding.Core.Attributes;
using DataBinding.Properties;
using UnityEngine;

/*</Using>*/

namespace /*<Namespace>*/Templates /*</Namespace>*/
{
    public /*<ClassName>*/abstract class ControllerClassTemplate /*</ClassName>*/ : Controller
    {
/*<FieldsAndMethods>*/
        /*<BoundProperty>*/
        [Binding(/*<PropertyName>*/"PropertyName" /*</PropertyName>*/), SerializeField]
        private BoundProperty</*<Type>*/bool /*</Type>*/> /*<Name>*/bindingFieldName /*</Name>*/;/*</BoundProperty>*/
        /*<PrivateField>*/
        private /*<Type>*/ Transform /*</Type>*/ /*<Name>*/t/*</Name>*/;/*</PrivateField>*/
        /*<BindingChangeMethod>*/
        [BindingChangeListener(/*<PropertyName>*/"PropertyPropertyName" /*</PropertyName>*/)] private void /*<Name>*/methodName/*</Name>*/()
        {
            /*<Body>*//*</Body>*/
        }/*</BindingChangeMethod>*/
/*</FieldsAndMethods>*/
        protected override void OnEnable()
        {
/*<CachedComponentExpression>*/
            /*<Name>*/t /*</Name>*/ = GetComponent</*<Type>*/Transform /*</Type>*/>();
/*</CachedComponentExpression>*/
            base.OnEnable();
        }
    }
}