using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Core.Attributes;
using DataBinding.Lib;
using DataBinding.Properties;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding
{
    [ExecuteAlways]
    public abstract class Controller : MonoBehaviour
    {
        private struct ListenerData
        {
            public IModel model;
            public string propertyName;
            public Action listener;
        }

        private List<ListenerData> listeners = new List<ListenerData>();
        private List<IBoundProperty> boundProperties = new List<IBoundProperty>();
        private bool isEnabled;

        protected virtual void OnEnable()
        {
            isEnabled = true;
            
            InitializeAnnotatedFields();
            ConnectChangeMethods();
        }

        protected virtual void OnDisable()
        {
            isEnabled = false;
            listeners.ForEach(data => { data.model.Unwatch(data.propertyName, data.listener); });
            listeners.Clear();

            boundProperties.ForEach(boundProperty => boundProperty.UnwatchAll());
            boundProperties.Clear();
        }

        protected virtual void OnTransformParentChanged()
        {
            if (!isEnabled) return;
            OnDisable();
            OnEnable();
        }

        private void ConnectChangeMethods()
        {
            var type = GetType();
            BindingReflectionUtils.GetAllMethods(type, info => true, true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .ToList().ForEach(info =>
                {
                    var attribute = info.GetCustomAttribute<BindingChangeListenerAttribute>();

                    var propertyName = attribute?.propertyName;
                    if (propertyName == null) return;

                    var parameters = info.GetParameters();
                    if (parameters.Length > 0)
                    {
                        throw new ArgumentException("Binding change listeners cannot have parameters");
                    }

                    AddPropertyListener(propertyName, info);
                });
        }

        private void InitializeAnnotatedFields()
        {
            var fieldInfos = BindingReflectionUtils.GetAllFields(GetType(), info => true, true, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .ToList();

            fieldInfos.ForEach(CacheComponents);
            fieldInfos.ForEach(ConnectControllerProperty);
            fieldInfos.ForEach(ConnectControllerPropertyChangeListeners);
        }

        private void ConnectControllerProperty(FieldInfo info)
        {
            var attribute = info.GetCustomAttribute<BindingAttribute>();
            var propertyName = attribute?.propertyName ?? "";
            if (!info.FieldType.IsGenericType || info.FieldType.GetGenericTypeDefinition() != typeof(BoundProperty<>)) return;

            IBoundProperty controllerProperty = info.GetValue(this) as IBoundProperty;
            if (controllerProperty == null)
            {
                // create new instance
                controllerProperty = Activator.CreateInstance(info.FieldType) as IBoundProperty;
                info.SetValue(this, controllerProperty);
            }

            boundProperties.Add(controllerProperty);
            controllerProperty.Connect(this, propertyName);
        }

        private void ConnectControllerPropertyChangeListeners(FieldInfo info)
        {
            var attribute = info.GetCustomAttribute<BindingAttribute>();
            var propertyName = attribute?.propertyName ?? "";
            if (!info.FieldType.IsGenericType
                || info.FieldType.GetGenericTypeDefinition() != typeof(BoundProperty<>)
                || string.IsNullOrEmpty(attribute.changeListener)) return;

            IBoundProperty controllerProperty = info.GetValue(this) as IBoundProperty;
            if (controllerProperty == null) return;

            var type = info.ReflectedType;
            var methodInfo = type.GetMethod(attribute.changeListener, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (methodInfo == null)
            {
                throw new ArgumentException($"Binding change listener method not found ({type.Name}.{attribute.changeListener})");
            }

            controllerProperty.Watch(() => methodInfo.Invoke(this, null));
        }

        private void CacheComponents(FieldInfo info)
        {
            info.GetCustomAttribute<GetComponentAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<GetComponentInChildrenAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<GetComponentsInChildrenAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<GetComponentInParentAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<GetComponentsInParentAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<FindAttribute>()?.TryInitialize(this, info, transform);
            info.GetCustomAttribute<GetChildAttribute>()?.TryInitialize(this, info, transform);
        }

        private void AddPropertyListener(string propertyName, MethodInfo info)
        {
            var model = BindingUtils.GetModel(this, propertyName);
            if (model == null) return;

            AddPropertyListener(propertyName, () => info.Invoke(this, null));
        }

        private void AddPropertyListener(string propertyName, Action listener)
        {
            var model = BindingUtils.GetModel(this, propertyName);
            if (model == null) return;

            listeners.Add(new ListenerData()
            {
                model = model,
                propertyName = propertyName,
                listener = listener
            });
            model.Watch(propertyName, listener);
        }

        #region UtilityMethods

        public void DestroyChildren()
        {
            for (var i = transform.childCount - 1; i >= 0; i--)
            {
                var child = transform.GetChild(i).gameObject;
                EditorUtils.Destroy(child);
            }
        }

        public Object InstantiatePrefab(GameObject prefab, Transform parent)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                return UnityEditor.PrefabUtility.InstantiatePrefab(prefab, parent);
            }
#endif

            return Instantiate(prefab, parent);
        }

        public Object InstantiatePrefab(GameObject prefab)
        {
            return InstantiatePrefab(prefab, transform);
        }

        #endregion
    }
}