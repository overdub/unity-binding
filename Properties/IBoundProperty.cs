using System;
using UnityEngine;

namespace DataBinding.Properties
{
    public interface IBoundProperty
    {
        void Connect(Component context, string propertyName = "");
        bool GetterConnected { get; }
        bool SetterConnected { get; }

        void Watch(Action listener);
        void Unwatch(Action listener);
        void UnwatchAll();
    }
}