using System;
using System.Collections.Generic;
using System.Reflection;
using DataBinding.Utils;
using Object = UnityEngine.Object;

namespace DataBinding.Properties
{
    [Serializable]
    public class WritablePropertyName
    {
        public string propertyName;

        public WritablePropertyName(string propertyName = "")
        {
            this.propertyName = propertyName;
        }

        public MemberInfo Resolve(Type targetType, Type preferredType)
        {
            return BindingReflectionUtils.GetMemberByName(targetType, preferredType, propertyName);
        }
        
        private sealed class PropertyNameEqualityComparer : IEqualityComparer<WritablePropertyName>
        {
            public bool Equals(WritablePropertyName x, WritablePropertyName y)
            {
                return x.propertyName == y.propertyName;
            }

            public int GetHashCode(WritablePropertyName obj)
            {
                return (obj.propertyName != null ? obj.propertyName.GetHashCode() : 0);
            }
        }

        public static IEqualityComparer<WritablePropertyName> PropertyNameComparer { get; } = new PropertyNameEqualityComparer();
    }
}