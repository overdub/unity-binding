using System;
using System.IO;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Properties
{
    [Serializable]
    public class RelativeObjectReference
    {
        [SerializeField] private string path;
        [SerializeField] private string assemblyName;
        [SerializeField] private string componentPath;

        public string ComponentPath => componentPath;

        public static RelativeObjectReference FromObject(Object target)
        {
            if (target is Component)
            {
                return new RelativeObjectReference("", ObjectUtils.GetComponentPath(target as Component), ObjectUtils.GetComponentAssembly(target as Component));
            }
            
            return new RelativeObjectReference("", "", "");
        }

        public RelativeObjectReference(string path, string componentPath, string assemblyName)
        {
            this.path = path;
            this.assemblyName = assemblyName;
            this.componentPath = componentPath;
        }

        public Object Resolve(Transform context)
        {
            var targetGameObject = ObjectUtils.ResolvePath(context, path);
            if (targetGameObject == null)
            {
                return null;
            }
    
            if (TargetIsGameObject)
            {
                return targetGameObject;
            }
            
            return ObjectUtils.ResolveComponentPath(context, componentPath, assemblyName);
        }

        public bool TargetIsGameObject => string.IsNullOrEmpty(componentPath) || componentPath == "GameObject";

        public Type TargetType => TargetIsGameObject ? typeof(GameObject) : ObjectUtils.ResolveComponentType(componentPath, assemblyName);
    }
}