using System;
using System.Reflection;
using DataBinding.Core;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

// TODO: create condition binding. e.g. property matches regexp => bool

namespace DataBinding.Properties
{
    [Serializable]
    public class Binding : IDisposable
    {
        [SerializeField] private DynamicModelProperty modelProperty;
        [SerializeField] private DynamicMemberAccessor accessor;

        public DynamicModelProperty ModelProperty
        {
            get => modelProperty;
            set
            {
                modelProperty = value;
                Recache();
            }
        }

        public DynamicMemberAccessor Accessor
        {
            get => accessor;
            set
            {
                accessor = value;
                Recache();
            }
        }

        private bool isValid;
        private bool isWatching;

        private Transform currentContext;
        private IModel model;

        private IPropertyTargetConnector connector;
        
        public Binding()
        {
        }

        public Binding(DynamicMemberAccessor accessor)
        {
            this.accessor = accessor;
        }

        public Binding(Object targetObject, string memberName)
        {
            accessor = new DynamicMemberAccessor(targetObject, memberName);
        }

        public bool Connect(Transform context, string propertyName, string componentPath, string assembly, string memberName)
        {
            modelProperty.PropertyName = propertyName;
            accessor.TargetObject = new RelativeObjectReference("", componentPath, assembly);
            accessor.Property = new WritablePropertyName(memberName);
            
            UpdateBinding(context);
            Recache();

            return isValid;
        }

        public void AutoFillModelProperty(Transform context)
        {
            if (!accessor.Connect(context)) return;

            var memberType = accessor.MemberType;
            
            foreach (var model in BindingUtils.GetParentModels(context))
            {
                foreach (var name in model.PropertyNames)
                {
                    if (model.GetPropertyType(name) == memberType)
                    {
                        modelProperty.PropertyName = name;
                        return;
                    } 
                }
            }
        }

        private void UpdateBinding(Transform context)
        {
            currentContext = context;
            
            modelProperty.Connect(context.transform);
            model = modelProperty.Model;

            isValid = context != null && model != null && !string.IsNullOrEmpty(modelProperty.PropertyName) && accessor.Connect(context.transform) && model.Has(modelProperty.PropertyName);
            if (!isValid)
            {
                Unwatch();
                return;
            }

            connector = PropertyTargetConnector.Connect(modelProperty.Model, modelProperty.Model.GetPropertyInfo(modelProperty.PropertyName), accessor.Target, accessor.MemberInfo);
        }

        public void Watch(Transform context)
        {
            Unwatch();

            UpdateBinding(context);
            if (!isValid) return;

            isWatching = true;

            model.Watch(modelProperty.PropertyName, connector.Apply);
        }

        public void Unwatch()
        {
            if (!isWatching) return;
            isWatching = false;

            if (modelProperty != null && connector != null)
            {
                model?.Unwatch(modelProperty.PropertyName, connector.Apply);
            }
        }

        public void Recache()
        {
            if (!isWatching) return;

            Unwatch();
            Watch(currentContext);
        }

        public void Dispose()
        {
            Unwatch();
        }
    }
}