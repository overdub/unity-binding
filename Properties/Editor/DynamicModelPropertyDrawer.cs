using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(DynamicModelProperty))]
    public class DynamicModelPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            var propertyName = property.FindPropertyRelative("propertyName");
            var propertyNames = BindingUtils.GetAvailableProperties((property.serializedObject.targetObject as Component).transform);
            
            // set warning background
            if (!propertyNames.Contains(propertyName.stringValue))
            {
                GUI.backgroundColor = BindingEditorUtils.WarningBgColor;
            }

            var propertyTarget = BindingEditorUtils.GetPropertyTargetObject(property);
            var placeholder = propertyTarget.GetType().GetCustomAttribute<PlaceholderAttribute>()?.placeholder ?? "Property";
            EditablePopup.Draw(position, label, propertyName, propertyNames, placeholder);
            GUI.backgroundColor = Color.white;
            
            EditorGUI.EndProperty();
        }
    }
}