using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(Binding))]
    public class BindingDrawer : PropertyDrawer
    {
        private float totalHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var parent = property.serializedObject.targetObject as MonoBehaviour;
            
            var top = position.y;

            position.height = EditorGUIUtility.singleLineHeight;
            if (label != GUIContent.none)
            {
                EditorGUI.PrefixLabel(position, label);
                position.x += EditorGUIUtility.labelWidth;
                position.width -= EditorGUIUtility.labelWidth;
            }
            
            var modelProperty = property.FindPropertyRelative("modelProperty");
            var accessorProperty = property.FindPropertyRelative("accessor");
            var accessor = BindingEditorUtils.GetPropertyTargetObject(accessorProperty) as DynamicMemberAccessor;
            var dynamicModelProperty = BindingEditorUtils.GetPropertyTargetObject(modelProperty) as DynamicModelProperty;
            
            EditorGUI.BeginChangeCheck();

            position.width /= 3;
            EditorGUI.PropertyField(position, modelProperty, GUIContent.none);

            position.x += position.width;
            position.width *= 2;

            if (!accessor.Connected)
            {
                accessor.Connect(parent);
            }
            if (!accessor.Connected)
            {
                GUI.color = BindingEditorUtils.WarningBgColor;
            }
            EditorGUI.PropertyField(position, accessorProperty, GUIContent.none);
            GUI.color = Color.white;

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
                
                accessor.inputType = dynamicModelProperty.PropertyType;

                var targetObject = BindingEditorUtils.GetPropertyTargetObject(property) as Binding;
                targetObject.Unwatch();
                targetObject.Watch(parent.transform);
            }

            totalHeight = position.y + position.height - top;

            GUI.backgroundColor = Color.white;
        }


        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return totalHeight;
        }
    }
}