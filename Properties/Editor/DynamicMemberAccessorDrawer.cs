using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(DynamicMemberAccessor))]
    public class DynamicMemberAccessorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var target = BindingEditorUtils.GetPropertyTargetObject(property) as DynamicMemberAccessor;
            
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            
            if (label != GUIContent.none)
            {
                EditorGUI.PrefixLabel(position, label);
                position.x += EditorGUIUtility.labelWidth;
                position.width -= EditorGUIUtility.labelWidth;
            }
            
            var rect = new Rect(position);
            rect.width = rect.width / 2 - 2;
            
            var targetObjectProperty = property.FindPropertyRelative("targetObject");
            EditorGUI.PropertyField(rect, targetObjectProperty, GUIContent.none);

            rect.x += rect.width + 2;
            var propertyReferenceProperty = property.FindPropertyRelative("property");

            var targetObject = (RelativeObjectReference) BindingEditorUtils.GetPropertyTargetObject(targetObjectProperty);
            var propertyNameProperty = propertyReferenceProperty.FindPropertyRelative("propertyName");
            var propertyNamePropertyTarget = BindingEditorUtils.GetPropertyTargetObject(propertyNameProperty);
            var placeholder = propertyNamePropertyTarget.GetType().GetCustomAttribute<PlaceholderAttribute>()?.placeholder ?? "Member name";
            WritablePropertyPopup.Draw(rect, targetObject.TargetType, target.inputType, propertyNameProperty, placeholder);

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
                target.Connect(property.serializedObject.targetObject as Component);
            }
            EditorGUI.EndProperty();
        }
    }
}