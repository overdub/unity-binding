using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(StringCompareBinding))]
    public class StringCompareBindingDrawer : PropertyDrawer
    {
        private static readonly Dictionary<StringCompareBinding.CompareOperation, string> compareOperationDisplayNames = new Dictionary<StringCompareBinding.CompareOperation, string>()
        {
            {StringCompareBinding.CompareOperation.Equal, "=="},
            {StringCompareBinding.CompareOperation.EqualIgnoreCase, "==i"},
            {StringCompareBinding.CompareOperation.Contains, "in"},
        };

        // TODO: Adjust for dark mode
        private Color warningYellow = new Color(1f, 0.91f, 0.65f);

        private struct BindingCache
        {
            public List<string> targetMemberNames;
        }

        private float totalHeight;
        private Dictionary<string, BindingCache> cache = new Dictionary<string, BindingCache>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var top = position.y;

            position.height = EditorGUIUtility.singleLineHeight;
            if (label != GUIContent.none)
            {
                EditorGUI.PrefixLabel(position, label);
                position.x += EditorGUIUtility.labelWidth;
                position.width -= EditorGUIUtility.labelWidth;
            }

            var propertyName = property.FindPropertyRelative("propertyName");
            var compareOperation = property.FindPropertyRelative("compareOperation");
            var compareValue = property.FindPropertyRelative("compareValue");
            var target = property.FindPropertyRelative("target");
            var targetMember = property.FindPropertyRelative("targetMember");
            var matchValueEnabled = property.FindPropertyRelative("matchValueEnabled");
            var matchValue = property.FindPropertyRelative("matchValue");
            var mismatchValueEnabled = property.FindPropertyRelative("mismatchValueEnabled");
            var mismatchValue = property.FindPropertyRelative("mismatchValue");

            var targetTransform = (property.serializedObject.targetObject as MonoBehaviour).transform;
            var model = BindingUtils.GetModel(property.serializedObject.targetObject as MonoBehaviour, propertyName.stringValue);

//            if (!this.cache.ContainsKey(property.propertyPath))
//            {
            var targetMembers = target.objectReferenceValue != null ? BindingReflectionUtils.GetMembersByType(target.objectReferenceValue.GetType(), model?.GetPropertyType(propertyName.stringValue)) : new List<MemberInfo>();

            this.cache[property.propertyPath] = new BindingCache()
            {
                targetMemberNames = targetMembers.Select(info => info.Name).ToList()
            };
//            }
            var cache = this.cache[property.propertyPath];

            EditorGUI.BeginChangeCheck();

            var parentModels = targetTransform != null ? targetTransform.GetComponentsInParent<IModel>() : new IModel[] { };
            var propertyNames = parentModels.ToList().SelectMany(m => m.PropertyNames).ToList();

            // set warning background
            if (!propertyNames.Contains(propertyName.stringValue))
            {
                GUI.backgroundColor = warningYellow;
            }

            position.width /= 6;
            EditablePopup.Draw(position, propertyName, propertyNames);

            position.x += position.width;
            DrawCompareValue(position, compareOperation, compareValue);

            position.x += position.width;
            EditorGUI.PropertyField(position, target, GUIContent.none);

            position.x += position.width;
            var targetMemberIndex = EditorGUI.Popup(position, cache.targetMemberNames.IndexOf(targetMember.stringValue), cache.targetMemberNames.ToArray());
            targetMember.stringValue = targetMemberIndex >= 0 ? cache.targetMemberNames[targetMemberIndex] : "";

            position.x += position.width;
            DrawTargetValue(position, "✓", matchValueEnabled, matchValue);

            position.x += position.width;
            DrawTargetValue(position, "✕", mismatchValueEnabled, mismatchValue);

            if (EditorGUI.EndChangeCheck())
            {
                this.cache.Remove(property.propertyPath);

                var targetObject = BindingEditorUtils.GetPropertyTargetObject(property) as StringCompareBinding;
                targetObject.Unwatch();
                targetObject.Watch(property.serializedObject.targetObject as MonoBehaviour);
            }

            totalHeight = position.y + position.height - top;

            GUI.backgroundColor = Color.white;
        }

        private void DrawTargetValue(Rect position, string toggleLabel, SerializedProperty enabledProperty, SerializedProperty targetValueProperty)
        {
            var toggleWidth = 24;
            var propertyRect = new Rect(position);
            propertyRect.width = toggleWidth;

            enabledProperty.boolValue = GUI.Toggle(propertyRect, enabledProperty.boolValue, toggleLabel, "Button");
            EditorGUI.BeginDisabledGroup(!enabledProperty.boolValue);
            propertyRect.x += toggleWidth;
            propertyRect.width = position.width - toggleWidth;
            EditorGUI.PropertyField(propertyRect, targetValueProperty, GUIContent.none);
            EditorGUI.EndDisabledGroup();
        }

        public static void DrawCompareValue(Rect position, SerializedProperty compareOperation, SerializedProperty compareValue)
        {
            var buttonWidth = 32;
            var propertyRect = new Rect(position);
            propertyRect.width = buttonWidth;

            compareOperation.enumValueIndex = EditorGUI.Popup(propertyRect, compareOperation.enumValueIndex, compareOperationDisplayNames.Values.ToArray(), "Button");

            propertyRect.x += buttonWidth;
            propertyRect.width = position.width - buttonWidth;
            compareValue.stringValue = EditorGUI.TextField(propertyRect, compareValue.stringValue);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return totalHeight;
        }
    }
}