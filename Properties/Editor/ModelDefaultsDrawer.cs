using DataBinding.Utils.Editor;
using Fluur.Lib.DataBinding.Properties;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(ModelDefaults))]
    public class ModelDefaultsDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            var rect = new Rect(position);

            rect.height = EditorGUIUtility.singleLineHeight;
            rect.width = EditorGUIUtility.labelWidth;
            EditorGUI.PrefixLabel(rect, label);
            rect.x += rect.width;

            var defaultsProperty = property.FindPropertyRelative("defaults");
            if (string.IsNullOrEmpty(defaultsProperty.stringValue))
            {
                GUI.enabled = false;
            }
            
            var inputWidth = position.width - rect.width;
            rect.width = inputWidth / 2 - 4;
            if (GUI.Button(rect, "Load"))
            {
                LoadDefaults(property);
            }
            
            GUI.enabled = true;
            
            rect.x += rect.width + 8;
            if (GUI.Button(rect, "Save"))
            {
                SaveDefaults(property);
            }
                
            rect.y += rect.height;
            rect.x = position.x;
            rect.width = position.width;
            EditorGUI.PropertyField(rect, property.FindPropertyRelative("resetOnAwake"));
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 3;
        }

        private void LoadDefaults(SerializedProperty property)
        {
            var model = property.serializedObject.targetObject as Model;
            var target = BindingEditorUtils.GetPropertyTargetObject(property) as ModelDefaults;
            
            target?.LoadDefaults(model);
        }

        private void SaveDefaults(SerializedProperty property)
        {
            var model = property.serializedObject.targetObject as Model;
            var target = BindingEditorUtils.GetPropertyTargetObject(property) as ModelDefaults;
            
            target?.SaveDefaults(model);
        }
    }
}