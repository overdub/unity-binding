using System.Collections.Generic;
using DataBinding.Lib.Editor;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(BindingsList))]
    public class BindingsListDrawer : PropertyDrawer
    {
        private Dictionary<string, EditableList<Binding>> reorderableLists = new Dictionary<string, EditableList<Binding>>();
        private ContextMenu contextMenu;
        private float totalHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var list = GetList(property);

            position.height = list.GetHeight();
            EditorGUI.BeginChangeCheck();
            list.DoList(position);
            if (EditorGUI.EndChangeCheck())
            {
                var context = property.serializedObject.targetObject as Component;
                var bindingsList = BindingEditorUtils.GetPropertyTargetObject(property) as BindingsList;
                
                bindingsList.Unwatch();
                property.serializedObject.ApplyModifiedProperties();
                bindingsList.Watch(context.transform);
            }
        }

        private EditableList<Binding> GetList(SerializedProperty property)
        {
            if (!reorderableLists.ContainsKey(property.propertyPath))
            {
                var newList = new EditableList<Binding>(property.serializedObject, property.FindPropertyRelative("bindings"));
                reorderableLists[property.propertyPath] = newList;
            }

            return reorderableLists[property.propertyPath];
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var list = GetList(property);

            return list.GetHeight();
        }
    }
}