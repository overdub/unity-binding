using System.Collections.Generic;
using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(RelativeObjectReference))]
    public class RelativeObjectReferenceDrawer : PropertyDrawer
    {
        private List<Component> components;
        private List<string> componentOptions;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            var target = property.serializedObject.targetObject as Component;

            var assemblyNameProperty = property.FindPropertyRelative("assemblyName");
            var componentProperty = property.FindPropertyRelative("componentPath");
            
            var propertyTarget = BindingEditorUtils.GetPropertyTargetObject(property);
            var placeholder = propertyTarget.GetType().GetCustomAttribute<PlaceholderAttribute>()?.placeholder ?? "Target";
            ComponentPopup.Draw(position, label.text, target.transform, componentProperty, assemblyNameProperty, placeholder);
            
            EditorGUI.EndProperty();
        }
    }
}