using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataBinding.Lib.Editor;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(WritablePropertyName))]
    public class WritablePropertyNameDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            var targetObject = property.serializedObject.targetObject;
            var target = (WritablePropertyName)BindingEditorUtils.GetPropertyTargetObject(property);
            
            var propertyNameProperty = property.FindPropertyRelative("propertyName");
            var placeholder = target.GetType().GetCustomAttribute<PlaceholderAttribute>()?.placeholder ?? "Member name";
            WritablePropertyPopup.Draw(position, label.text, targetObject.GetType(), null, propertyNameProperty, placeholder);
            
            EditorGUI.EndProperty();
        }
    }
}