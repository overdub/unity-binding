using System.Reflection;
using DataBinding.Core.Attributes;
using DataBinding.Lib.Editor;
using DataBinding.Utils;
using DataBinding.Utils.Editor;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace DataBinding.Properties.Editor
{
    [CustomPropertyDrawer(typeof(BoundProperty<>))]
    public class BoundPropertyDrawer : PropertyDrawer
    {
        private GUIStyle annotationStyle;
        private Color activeColor = new Color(0.2f, 0.6f, 0.4f);
        private Color inactiveColor = new Color(0.8f, 0.2f, 0.2f);
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Init();
            
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();
            
            var target = BindingEditorUtils.GetPropertyTargetObject(property) as IBoundProperty;
            var type = target.GetType();
            var valueProperty = type.GetProperty("Value");
            var valueField = type.GetField("value", BindingFlags.Instance | BindingFlags.NonPublic);
            valueField.SetValue(target, valueProperty.GetValue(target));

            var propertyNameSerializedProperty = property.FindPropertyRelative("propertyName");
            var valueSerializedProperty = property.FindPropertyRelative("value");

            DrawAnnotation(position, "I", 0, target.GetterConnected ? activeColor : inactiveColor);
            DrawAnnotation(position, "O", annotationStyle.fontSize, target.SetterConnected ? activeColor : inactiveColor);

            GUI.color = !target.GetterConnected && !target.SetterConnected ? BindingEditorUtils.WarningBgColor : Color.white;

            // TODO: Fix issue where value field does update only after event
            var bindingAttribute = fieldInfo.GetCustomAttribute<BindingAttribute>();
            if (string.IsNullOrEmpty(bindingAttribute?.propertyName))
            {
                if (!string.IsNullOrEmpty(label.text))
                {
                    EditorGUI.PrefixLabel(position, label);
                }
                
                var rect = new Rect(position);
                rect.x += EditorGUIUtility.labelWidth;
                rect.width = (position.width - EditorGUIUtility.labelWidth - EditorGUIUtility.standardVerticalSpacing) * 0.5f;
                var propertyNames = BindingUtils.GetAvailableProperties((property.serializedObject.targetObject as Component).transform, valueField.FieldType);
                
                EditorGUI.BeginChangeCheck();
                EditablePopup.Draw(rect, propertyNameSerializedProperty, propertyNames, "Property");
                if (EditorGUI.EndChangeCheck())
                {
                    property.serializedObject.ApplyModifiedProperties();
                    target.Connect(property.serializedObject.targetObject as Component, propertyNameSerializedProperty.stringValue);
                }
                
                rect.x += rect.width + EditorGUIUtility.standardVerticalSpacing * 0.5f;
                EditorGUI.PropertyField(rect, valueSerializedProperty, GUIContent.none);
            } else
            {
                propertyNameSerializedProperty.stringValue = bindingAttribute.propertyName;
                property.serializedObject.ApplyModifiedProperties();
                EditorGUI.PropertyField(position, valueSerializedProperty, label);
            }
             
            GUI.color = Color.white;
            
            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();

                var newValue = valueField.GetValue(target);
                valueProperty.SetValue(target, newValue);
            }
            
            EditorGUI.EndProperty();
            
            property.serializedObject.Update();
        }

        private void Init()
        {
            if (annotationStyle != null) return;
            
            annotationStyle = new GUIStyle(EditorStyles.miniLabel)
            {
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.UpperCenter,
                fontSize = Mathf.RoundToInt(EditorStyles.miniLabel.fontSize * 0.75f),
                normal =
                {
                    textColor = Color.white
                }
            };
        }

        private void DrawAnnotation(Rect position, string label, float top, Color color)
        {
            var rect = new Rect(position);
            rect.x -= 16;
            rect.y += top;
            rect.width = 16;
            
            GUI.color = color;
            GUI.Label(rect, label, annotationStyle);
            GUI.color = Color.white;
        }
    }
}