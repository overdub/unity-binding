using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBinding.Utils;
using UnityEngine;

namespace DataBinding.Properties
{
    [Serializable]
    public class BoundProperty<T> : IBoundProperty
    {
        public bool GetterConnected { get; private set; }
        public bool SetterConnected { get; private set; }

        [SerializeField] private T value;
        [SerializeField] private string propertyName;

        private Func<T> propertyGetter;
        private Action<T> propertySetter;

        private IModel model;
        private List<Action> listeners = new List<Action>();

        public string PropertyName
        {
            get => propertyName;
        }

        public BoundProperty()
        {
            propertyGetter = () => value;
            propertySetter = v => value = v;
        }

        public T Value
        {
            get { return propertyGetter.Invoke(); }
            set { propertySetter.Invoke(value); }
        }

        public void Connect(Component context, string propertyName = "")
        {
            UnwatchAll();

            this.propertyName = !string.IsNullOrEmpty(propertyName) ? propertyName : this.propertyName;

            model = BindingUtils.GetModel(context, this.propertyName);

            var propertyInfo = model?.GetPropertyInfo(this.propertyName);
            GetterConnected = propertyInfo?.CanRead ?? false;
            SetterConnected = propertyInfo?.CanWrite ?? false;

            propertyGetter = GetterConnected ? BindingReflectionUtils.CreateTypedModelPropertyGetter<T>(model, this.propertyName) : () => value;
            propertySetter = SetterConnected ? BindingReflectionUtils.CreateTypedModelPropertySetter<T>(model, this.propertyName) : v => value = v;

            value = propertyGetter.Invoke();
        }

        public void Watch(Action listener)
        {
            if (model == null) return;

            model.Watch(propertyName, listener);
            listeners.Add(listener);
        }

        public void WatchOnce(Action listener)
        {
            if (model == null) return;

            model.Watch(propertyName, listener, () =>
            {
                listener.Invoke();
                Unwatch(listener);
            }, false);
            listeners.Add(listener);
        }

        public void WatchForValue(T requestedValue, Action listener)
        {
            if (model == null) return;

            model.Watch(propertyName, listener, () =>
            {
                if (!Value.Equals(requestedValue)) return;
                listener.Invoke();
            }, true);
            listeners.Add(listener);
        }
        
        public void WatchForValueOnce(T requestedValue, Action listener)
        {
            if (model == null) return;

            model.Watch(propertyName, listener, () =>
            {
                if (!Value.Equals(requestedValue)) return;
                Unwatch(listener);
                listener.Invoke();
            }, true);
            listeners.Add(listener);
        }

        public async Task<T> WaitForChange()
        {
            var completion = new TaskCompletionSource<T>();
            WatchOnce(() => completion.SetResult(Value));
            return await completion.Task;
        }
        
        public async Task<T> WaitForValue(T requestedValue)
        {
            var completion = new TaskCompletionSource<T>();
            WatchForValueOnce(requestedValue, () => completion.SetResult(Value));
            return await completion.Task;
        }
        
        public void Unwatch(Action listener)
        {
            if (model == null) return;

            model.Unwatch(propertyName, listener);
            listeners.Remove(listener);
        }

        public void UnwatchAll()
        {
            listeners.ForEach(listener => model?.Unwatch(propertyName, listener));
            listeners.Clear();
        }
    }
}