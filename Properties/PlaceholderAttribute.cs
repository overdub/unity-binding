using System;

namespace DataBinding.Properties
{
    [AttributeUsage(AttributeTargets.Field)]
    public class PlaceholderAttribute : Attribute
    {
        public string placeholder;

        public PlaceholderAttribute(string placeholder)
        {
            this.placeholder = placeholder;
        }
    }
}