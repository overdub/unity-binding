using System;
using DataBinding;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

//TODO: JSONUtility can't serialize Object references - other serialization methods?
namespace Fluur.Lib.DataBinding.Properties
{
    [Serializable]
    public class ModelDefaults
    {
        public bool resetOnAwake = true;
        [SerializeField, HideInInspector] private string defaults;

        #region defaults

        public void LoadDefaults(Model target)
        {
            var cachedDefaults = defaults;
            JsonUtility.FromJsonOverwrite(defaults, target);
            defaults = cachedDefaults;

#if UNITY_EDITOR
            EditorUtility.SetDirty(target);
#endif
        }

        public void SaveDefaults(Model target)
        {
            defaults = "";
            defaults = JsonUtility.ToJson(target);

#if UNITY_EDITOR
            EditorUtility.SetDirty(target);
#endif
        }

        #endregion
    }
}