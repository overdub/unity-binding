using System;
using System.Collections.Generic;
using DataBinding.Utils;
using UnityEngine;

namespace DataBinding.Properties
{
    [Serializable]
    public class DynamicModelProperty
    {
        [SerializeField] private string propertyName;

        public IModel Model { get; private set; }

        private List<Action> listeners = new List<Action>();

        private bool connected;
        private Transform context;

        public bool Connected => connected;

        public event Action changed;
        public event Action reconnected;

        public string PropertyName
        {
            get => propertyName;
            set
            {
                if (propertyName == value) return;
                propertyName = value;
                
                changed?.Invoke();
                if (connected)
                {
                    Connect(context);
                }
            }
        }

        public Type PropertyType => connected ? Model.GetPropertyType(propertyName) : null; 

        public bool Connect(Transform context)
        {
            connected = false;
            Model = null;

            if (context == null) return false;

            var model = BindingUtils.GetModel(context, propertyName);
            if (model == null) return false;

            Model = model;
            this.context = context;

            connected = true;
            reconnected?.Invoke();
            return true;
        }

        public void Watch(Action listener)
        {
            if (Model == null) return;

            Model.Watch(PropertyName, listener);
            listeners.Add(listener);
        }

        public void Unwatch(Action listener)
        {
            if (Model == null) return;

            Model.Unwatch(PropertyName, listener);
            listeners.Remove(listener);
        }

        public void UnwatchAll()
        {
            listeners.ForEach(listener => Model?.Unwatch(PropertyName, listener));
            listeners.Clear();
        }

        public Func<object> GetUntypedGetter()
        {
            if (!connected)
            {
                return () => default;
            }
            
            return BindingReflectionUtils.CreateUntypedModelPropertyGetter(Model, propertyName);
        }

        public Func<T> GetTypedGetter<T>()
        {
            if (!connected)
            {
                return () => default;
            }
            
            return BindingReflectionUtils.CreateTypedModelPropertyGetter<T>(Model, propertyName);
        }

        public Action<T> GetTypedSetter<T>()
        {
            if (!connected)
            {
                return value => { };
            }
            
            return BindingReflectionUtils.CreateTypedModelPropertySetter<T>(Model, propertyName);
        }
    }
}