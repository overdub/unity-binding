using System;
using System.Collections.Generic;
using UnityEngine;

namespace DataBinding.Properties
{
    [Serializable]
    public class BindingsList
    {
        public List<Binding> bindings = new List<Binding>();
        
        public void Watch(Transform context)
        {
            bindings.ForEach(binding => binding.Watch(context));
        }

        public void Unwatch()
        {
            bindings.ForEach(binding => binding.Unwatch());
        }
    }
}