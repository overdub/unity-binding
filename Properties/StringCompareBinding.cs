using System;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Properties
{
    [Serializable]
    public class StringCompareBinding
    {
        public enum CompareOperation
        {
            Equal,
            EqualIgnoreCase,
            Contains
        }

        public CompareOperation compareOperation;
        public string compareValue;
        public bool matchValueEnabled = true;
        public string matchValue;
        public bool mismatchValueEnabled = true;
        public string mismatchValue;

        [SerializeField] private string propertyName;
        [SerializeField] private Object target;
        [SerializeField] private string targetMember;

        private bool isValid;
        private bool isWatching;

        private IModel model;

        private Func<string> getter;
        private Action<string> setter;

        private Type propertyType;

        public void UpdateBinding(MonoBehaviour parent)
        {
            model = BindingUtils.GetModel(parent, propertyName);

            isValid = parent != null && model != null && !string.IsNullOrEmpty(propertyName) && target != null && model.Has(propertyName);
            if (!isValid)
            {
                Unwatch();
                return;
            }

            propertyType = model.GetPropertyType(propertyName);
            var memberInfo = BindingReflectionUtils.GetMemberByName(target.GetType(), propertyType, targetMember);
            var memberType = BindingReflectionUtils.GetMemberInputType(memberInfo);

            if (propertyType != typeof(string) || memberType != typeof(string))
            {
                isValid = false;
                Unwatch();
                return;
            }

            getter = BindingReflectionUtils.CreateTypedModelPropertyGetter<string>(model, propertyName);
            setter = BindingReflectionUtils.CreateTypedSetter<string>(target, memberInfo);
        }


        private bool Compare(string value)
        {
            switch (compareOperation)
            {
                case CompareOperation.Equal:
                    return string.Equals(value, compareValue, StringComparison.InvariantCulture);
                case CompareOperation.EqualIgnoreCase:
                    return string.Equals(value, compareValue, StringComparison.InvariantCultureIgnoreCase);
                case CompareOperation.Contains:
                    return value.Contains(compareValue);
            }

            return false;
        }

        private void ValueChanged()
        {
            if (!isWatching || model == null || !isValid) return;

            var value = getter();
            var match = value != null && Compare(value);

            if (match && matchValueEnabled)
            {
                setter(matchValue);
            }

            if (!match && mismatchValueEnabled)
            {
                setter(mismatchValue);
            }
        }

        public void Watch(MonoBehaviour parent)
        {
            Unwatch();

            UpdateBinding(parent);
            if (!isValid) return;

            isWatching = true;

            model.Watch(propertyName, ValueChanged);
        }

        public void Unwatch()
        {
            isWatching = false;
            if (model == null) return;

            model.Unwatch(propertyName, ValueChanged);
        }
    }
}