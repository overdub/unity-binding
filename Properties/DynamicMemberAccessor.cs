using System;
using System.Reflection;
using DataBinding.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DataBinding.Properties
{
    [Serializable]
    public class DynamicMemberAccessor
    {
        [SerializeField] private RelativeObjectReference targetObject;
        [SerializeField] private WritablePropertyName property;
        
        private Component context;

        public Type inputType;

        private bool connected;
        private MemberInfo memberInfo;
        private Object target;
        private Type memberType;

        public bool Connected => connected;
        public event Action reconnected;
        
        public RelativeObjectReference TargetObject
        {
            get => targetObject;
            set
            {
                targetObject = value;
                Connect();
            }
        }

        public WritablePropertyName Property
        {
            get => property;
            set
            {
                if (property.Equals(value)) return;
                property = value;

                Connect();
            }
        }

        public DynamicMemberAccessor()
        {
        }

        public DynamicMemberAccessor(Type inputType)
        {
            this.inputType = inputType;
        }
        
        public DynamicMemberAccessor(Object target, string memberName)
        {
            targetObject = RelativeObjectReference.FromObject(target);
            property.propertyName = memberName;
        }

        public bool Connect(Component currentContext = null)
        {
            connected = false;
            memberInfo = null;
            memberType = null;
            
            if (currentContext != null)
            {
                context = currentContext;
            }

            if (context == null)
            {
                return false;
            }
            
            target = targetObject.Resolve(context.transform);
            if (target == null)
            {
                return false;
            }
            memberInfo = property.Resolve(targetObject.TargetType, inputType);
            if (memberInfo == null)
            {
                return false;
            }
            memberType = BindingReflectionUtils.GetMemberInputType(memberInfo);
            
            connected = memberInfo != null;
            if (connected)
            {
                reconnected?.Invoke();
            }
            
            return connected;
        }

        public Object Target => target;
        
        public MemberInfo MemberInfo => memberInfo;
        
        public Type MemberType => memberType;

        public Action<T> GetTypedSetter<T>()
        {
            if (!connected && !Connect())
            {
                Debug.LogWarning("Could not create typed setter");
                return value => {};
            }

            return BindingReflectionUtils.CreateTypedSetter<T>(target, memberInfo);
        }
    }
}