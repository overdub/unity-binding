﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using DataBinding.Core;
using Fluur.Lib.DataBinding.Properties;
using UnityEngine;
#if UNITY_EDITOR
using DataBinding.Utils;
using UnityEditor;

#endif

namespace DataBinding
{
    [ExecuteAlways, DefaultExecutionOrder(-100)]
    public abstract class Model : MonoBehaviour, ISerializationCallbackReceiver, IModel
    {
        public event Action<string> PropertyChanged;

        [SerializeField] private ModelDefaults defaults = null;

        private bool initialized;
        private PropertyCache propertyCache;
        private readonly WatcherList watcherList = new WatcherList();

        protected void SetPropertyValue<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                Undo.RecordObject(this, "Undo property change");
            }
#endif

            field = value;

            // TODO: take care of submodules
            DispatchChange(propertyName);
        }

        protected void DispatchChange(string propertyName)
        {
            PropertyChanged?.Invoke(propertyName);
            watcherList.Dispatch(propertyName);
        }

        public bool Has(string propertyName)
        {
            Init();

            return propertyCache.Has(propertyName);
        }

        public Type GetPropertyType(string propertyName)
        {
            Init();

            return propertyCache.GetPropertyType(propertyName);
        }

        public List<string> PropertyNames
        {
            get
            {
                Init();

                return propertyCache.PropertyNames;
            }
        }

        public object Get(string property)
        {
            Init();

            return propertyCache.Get(property);
        }

        public PropertyInfo GetPropertyInfo(string property)
        {
            Init();

            return propertyCache.GetPropertyInfo(property);
        }

        public object GetPropertyTarget(string propertyName)
        {
            return propertyCache.GetPropertyTarget(propertyName);
        }

        public void Watch(string property, Action listener)
        {
            Init();

            watcherList.Watch(property, listener);
            listener?.Invoke();
        }
        
        public void Watch(string property, Action listener, Action wrapper, bool initialDispatch)
        {
            Init();

            watcherList.Watch(property, listener, wrapper);
            if (initialDispatch)
            {
                wrapper.Invoke();
            }
        }
        
        public void Unwatch(string property, Action listener)
        {
            Init();

            watcherList.Unwatch(property, listener);
        }

        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            StartObservingChanges();
#endif
        }

        public void OnAfterDeserialize()
        {
#if UNITY_EDITOR
            StopObservingChanges();
#endif
        }

        protected virtual void Recache()
        {
            initialized = true;

            propertyCache = new PropertyCache(this);
        }

        private void Init()
        {
            if (initialized && propertyCache?.PropertyCount > 0) return;

            Recache();
        }

        private void UpdateSerializedFields()
        {
            Init();

            var propertyNames = propertyCache.PropertyNames;
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(propertyName);
                watcherList.Dispatch(propertyName);
            }
        }

        protected virtual void OnDestroy()
        {
            watcherList.UnwatchAll();
            initialized = false;

#if UNITY_EDITOR
            StopObservingChanges();
#endif
        }

        protected virtual void Awake()
        {
            Recache();

            if (Application.isPlaying && defaults.resetOnAwake)
            {
                LoadDefaults();
            }

            UpdateSerializedFields();
        }

        public void LoadDefaults()
        {
            defaults.LoadDefaults(this);
        }

#if UNITY_EDITOR
        private void StartObservingChanges()
        {
            StopObservingChanges();
            Undo.postprocessModifications += PostprocessModificationsCallback;
        }

        private void StopObservingChanges()
        {
            Undo.postprocessModifications -= PostprocessModificationsCallback;
        }

        UndoPropertyModification[] PostprocessModificationsCallback(UndoPropertyModification[] modifications)
        {
            if (this == null) return modifications;

            Init();

            modifications.ToList().ForEach(modification =>
            {
                if (modification.previousValue.target != this) return;
                if (modification.previousValue.value.Equals(modification.currentValue.value)) return;

                // parse field property path to extract serialized field name
                var fieldName = BindingReflectionUtils.GetPropertyPathRoot(modification.currentValue.propertyPath);

                var propertyName = propertyCache.FindPropertyByFieldName(fieldName);
                if (propertyName != null)
                {
                    DispatchChange(propertyName);
                }
            });

            return modifications;
        }
#endif
    }
}